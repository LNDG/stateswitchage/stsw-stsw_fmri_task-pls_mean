% extract probability locations using the Anatomy toolbox

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
rootpath = pwd;

pn.data = fullfile(rootpath, 'data', 'pls');
pn.spm = fullfile(rootpath, 'tools', 'spm12'); addpath(pn.spm);

%% behavioral PLS: LV1

% export peak locations
load(fullfile(pn.data, 'behavPLS_STSWD_SPM_YA_OA_red_1group_3mm_1000P1000B_BfMRIcluster.mat'))
locs = cluster_info.data{1}.peak_loc;
fid = fopen(fullfile(pn.data, 'behavPLS_STSWD_SPM_YA_OA_red_1group_3mm_1000P1000B_BfMRIcluster_forSPM.txt'),'wt');
for ii = 1:size(locs,1)
    fprintf(fid,'%g\t',locs(ii,:));
    fprintf(fid,'\n');
end
fclose(fid)

% export BSR and voxel amount
BSRval = cluster_info.data{1}.peak_values';
BSRval = round(BSRval*100)/100;
BSRval = [BSRval, cluster_info.data{1}.size'];
fid = fopen(fullfile(pn.data, 'behavPLS_STSWD_SPM_YA_OA_red_1group_3mm_1000P1000B_BfMRIcluster_BSR.txt'),'wt');
for ii = 1:size(BSRval,1)
    fprintf(fid,'%g\t',BSRval(ii,:));
    fprintf(fid,'\n');
end
fclose(fid)

%% task PLS: LV1

files = {'behavPLS_STSWD_SPM_YA_OA_red_1group_3mm_1000P1000B_BfMRIcluster'; ...
    'taskPLS_STSWD_SPM_YA_OA_3mm_1000P1000B_lv1_BfMRIcluster'; ...
    'taskPLS_STSWD_SPM_YA_OA_3mm_1000P1000B_lv2_BfMRIcluster'; ...
    'taskPLS_STSWD_SPM_YA_OA_3mm_1000P1000B_lv3_BfMRIcluster'};

for indFile = 1:numel(files)
    % export peak locations
    load(fullfile(pn.data, [files{indFile}, '.mat']))
    locs = cluster_info.data{1}.peak_loc;
    fid = fopen(fullfile(pn.data, [files{indFile}, '_forSPM.txt']),'wt');
    for ii = 1:size(locs,1)
        fprintf(fid,'%g\t%g\t%g',locs(ii,1), locs(ii,2), locs(ii,3));
        fprintf(fid,'\n');
    end
    fclose(fid)
    % export BSR and voxel amount
    BSRval = cluster_info.data{1}.peak_values';
    BSRval = round(BSRval*100)/100;
    BSRval = [BSRval, cluster_info.data{1}.size'];
    fid = fopen(fullfile(pn.data, [files{indFile}, '_BSR.txt']),'wt');
    for ii = 1:size(BSRval,1)
        fprintf(fid,'%g\t%g',BSRval(ii,1), BSRval(ii,2));
        fprintf(fid,'\n');
    end
    fclose(fid)
end

%% external: batch process the peak locations
% requires the spm anatomy toolbox

spm fmri