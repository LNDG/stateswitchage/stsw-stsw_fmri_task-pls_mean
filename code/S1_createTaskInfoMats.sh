#!/bin/bash

# check [module avail matlab] and choose matlab version

fun_name="S1_createTaskInfoMats"

module load matlab/R2016b
matlab -nodesktop -nosplash -r "run ${fun_name}.m"
exit