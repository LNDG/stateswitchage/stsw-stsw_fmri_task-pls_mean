#!/bin/bash

# setup tools

rootpath="$(pwd)/.."
rootpath=$(builtin cd $rootpath; pwd)

cd ${rootpath}/tools/

git clone git@git.mpib-berlin.mpg.de:LNDG/preprocessing_tools.git
git clone git@git.mpib-berlin.mpg.de:LNDG/nifti_toolbox.git
git clone git@git.mpib-berlin.mpg.de:LNDG/pls.git
git clone git@github.com:raacampbell/shadedErrorBar.git
git clone git@github.com:lindenmp/rs-fMRI.git