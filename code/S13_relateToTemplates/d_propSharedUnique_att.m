% plot loadings on unique and shared attention/uncertainty activation

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..', '..'))
rootpath = pwd;

pn.data = fullfile(rootpath, 'data');
pn.tools = fullfile(rootpath, 'tools'); 
    addpath(genpath(fullfile(pn.tools, 'nifti_toolbox')));
    addpath(genpath(fullfile(pn.tools, 'preprocessing_tools')));
pn.figures = fullfile(rootpath, 'figures', 'S13');

% load BSR
BSR_img_unthresh = S_load_nii_2d(fullfile(pn.data, ...
    'references','behavPLS_STSWD_SPM_YA_OA_red_1group_3mm_1000P1000B_BfMRIbsr_lv1_unthresholded_thr_mask.nii.gz'))';
BSR_img_unthresh(BSR_img_unthresh==0) = NaN;

% load Muller
Muller = S_load_nii_2d(fullfile(pn.data, ...
    'references','Fig1_supp3a.nii'))';
Muller(Muller==0) = NaN;

% load shared
SHARE = S_load_nii_2d(fullfile(pn.data, ...
    'references','attention_uncertainty_binary.nii.gz'))';
SHARE(SHARE==0) = NaN;

% load unique uncertainty
UNCERT = S_load_nii_2d(fullfile(pn.data, ...
    'references','uncertainty_uniformity-test_z_FDR_0.01_binary_att_unique.nii.gz'))';
UNCERT(UNCERT==0) = NaN;

% load unique attention
attention = S_load_nii_2d(fullfile(pn.data, ...
    'references','attention_uniformity-test_z_FDR_0.01_binary_unique.nii.gz'))';
attention(attention==0) = NaN;

%% get overlap

loadings_shared = nanmean(abs(BSR_img_unthresh(SHARE==1)));
loadings_uncertainty = nanmean(abs(BSR_img_unthresh(UNCERT==1)));
loadings_shared_uncertainty = nanmean(abs(BSR_img_unthresh(SHARE==1|UNCERT==1)));
loadings_attention = nanmean(abs(BSR_img_unthresh(attention==1)));

loadingsMul_shared = nanmean(abs(Muller(SHARE==1)));
loadingsMul_uncertainty = nanmean(abs(Muller(UNCERT==1)));
loadingsMul_shared_uncertainty = nanmean(abs(Muller(SHARE==1|UNCERT==1)));
loadingsMul_attention = nanmean(abs(Muller(attention==1)));
% 
% h = figure('units','centimeters','position',[.1 .1 8 6]);
% set(gcf,'renderer','Painters')
% cla; hold on;
% bar([loadings_shared, loadings_uncertainty, loadings_attention, ...
%     loadingsMul_shared, loadingsMul_uncertainty, loadingsMul_attention])

h = figure('units','centimeters','position',[.1 .1 12 8]);
set(gcf,'renderer','Painters')
subplot(1,2,1);
    cla; hold on;
    bar([loadings_shared_uncertainty, loadings_attention], 'FaceColor', 'black')
    ylabel({'abs. BSR'})
    ylim([0 3.5])
    xticks(1:2)
    xticklabels({'uncertainty'; 'attention (unique)'})
    xtickangle(90)
    title({'Task uncertainty';'(Kosciessa et al.,)'})
subplot(1,2,2);
    cla; hold on;
    bar([loadingsMul_shared_uncertainty, loadingsMul_attention], 'FaceColor', 'black')
    ylabel({'abs. t-value'})
    ylim([0 3.5])
    xticks(1:2)
    xticklabels({'uncertainty'; 'attention (unique)'})
    xtickangle(90)
    title({'State uncertainty';'(Muller et al.,)'})
set(findall(gcf,'-property','FontSize'),'FontSize',13)

figureName = 'attention_uncertainty';
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

%% do the same for taskPLS loadings

taskPLS1 = S_load_nii_2d(fullfile(pn.data, ...
    'references','taskPLS_STSWD_SPM_YA_OA_3mm_1000P1000B_BfMRIbsr_lv1_thr_mask.nii.gz'))';
taskPLS1(taskPLS1==0) = NaN;

taskPLS2 = S_load_nii_2d(fullfile(pn.data, ...
    'references','taskPLS_STSWD_SPM_YA_OA_3mm_1000P1000B_BfMRIbsr_lv2_thr_mask.nii.gz'))';
taskPLS2(taskPLS2==0) = NaN;

taskPLS3 = S_load_nii_2d(fullfile(pn.data, ...
    'references','taskPLS_STSWD_SPM_YA_OA_3mm_1000P1000B_BfMRIbsr_lv3_thr_mask.nii.gz'))';
taskPLS3(taskPLS3==0) = NaN;

%% get overlap

loadings_shared = nanmean(abs(BSR_img_unthresh(SHARE==1)));
loadings_uncertainty = nanmean(abs(BSR_img_unthresh(UNCERT==1)));
loadings_shared_uncertainty = nanmean(abs(BSR_img_unthresh(SHARE==1|UNCERT==1)));
loadings_attention = nanmean(abs(BSR_img_unthresh(attention==1)));

pls1_shared = nanmean(abs(taskPLS1(SHARE==1)));
pls1_uncertainty = nanmean(abs(taskPLS1(UNCERT==1)));
pls1_shared_uncertainty = nanmean(abs(taskPLS1(SHARE==1|UNCERT==1)));
pls1_attention = nanmean(abs(taskPLS1(attention==1)));

pls2_shared = nanmean(abs(taskPLS2(SHARE==1)));
pls2_uncertainty = nanmean(abs(taskPLS2(UNCERT==1)));
pls2_shared_uncertainty = nanmean(abs(taskPLS2(SHARE==1|UNCERT==1)));
pls2_attention = nanmean(abs(taskPLS2(attention==1)));

pls3_shared = nanmean(abs(taskPLS3(SHARE==1)));
pls3_uncertainty = nanmean(abs(taskPLS3(UNCERT==1)));
pls3_shared_uncertainty = nanmean(abs(taskPLS3(SHARE==1|UNCERT==1)));
pls3_attention = nanmean(abs(taskPLS3(attention==1)));

h = figure('units','centimeters','position',[.1 .1 12 8]);
set(gcf,'renderer','Painters')
subplot(1,2,1);
    cla; hold on;
    bar([pls1_shared,  pls2_shared, pls3_shared; ...
        pls1_uncertainty, pls2_uncertainty, pls3_uncertainty; ...
        pls1_attention, pls2_attention, pls3_attention], 'FaceColor', 'black')
    ylabel({'abs. BSR'})
    %ylim([0 3.5])
    xticks(1:3)
    xticklabels({'shared'; 'uncertainty'; 'attention (unique)'})
    xtickangle(90)
