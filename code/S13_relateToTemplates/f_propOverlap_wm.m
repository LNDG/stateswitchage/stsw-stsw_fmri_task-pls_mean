% plot loadings on unique and shared attention/uncertainty activation

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..', '..'))
rootpath = pwd;

pn.data = fullfile(rootpath, 'data');
pn.tools = fullfile(rootpath, 'tools'); 
    addpath(genpath(fullfile(pn.tools, 'nifti_toolbox')));
    addpath(genpath(fullfile(pn.tools, 'preprocessing_tools')));
pn.figures = fullfile(rootpath, 'figures', 'S13');

% load BSR
BSR_img_unthresh = S_load_nii_2d(fullfile(pn.data, ...
    'references','behavPLS_STSWD_SPM_YA_OA_red_1group_3mm_1000P1000B_BfMRIbsr_lv1_unthresholded_thr_mask.nii.gz'))';

% load Muller
Muller = S_load_nii_2d(fullfile(pn.data, ...
    'references','Fig1_supp3a.nii'))';

% load shared
SHARE = S_load_nii_2d(fullfile(pn.data, ...
    'references','wm_uncertainty_binary.nii.gz'))';

% load unique uncertainty
UNCERT = S_load_nii_2d(fullfile(pn.data, ...
    'references','uncertainty_uniformity-test_z_FDR_0.01_binary_wm_unique.nii.gz'))';

% load unique WM
WM = S_load_nii_2d(fullfile(pn.data, ...
    'references','workingmemory_uniformity-test_z_FDR_0.01_binary_unique.nii.gz'))';

%% binarize BSR, Muller

BSR_img_unthresh(BSR_img_unthresh>-1.96 & BSR_img_unthresh<1.96) =NaN;
BSR_img_unthresh(~isnan(BSR_img_unthresh)) = 1;
BSR_img_unthresh(isnan(BSR_img_unthresh)) = 0;

Muller(Muller>-1.96 & Muller<1.96) = NaN;
Muller(~isnan(Muller)) = 1;
Muller(isnan(Muller)) = 0;

%% get overlap (% of voxels in Neurosynth maps)

% of shared

idx_share = find(SHARE==1);
idx_TU = find(BSR_img_unthresh==1);
idx_share_TU = find(SHARE & BSR_img_unthresh);
disp(['percentage shared:', num2str(round(numel(idx_share_TU)/numel(idx_share)*100,1))])

% of unique uncertainty

idx_uncert = find(UNCERT==1);
idx_uncert_TU = find(UNCERT & BSR_img_unthresh);
disp(['percentage unique uncertainty:', num2str(round(numel(idx_uncert_TU)/numel(idx_uncert)*100,1))])

% of unique wm

idx_wm = find(WM==1);
idx_wm_TU = find(WM & BSR_img_unthresh);
disp(['percentage unique wm:', num2str(round(numel(idx_wm_TU)/numel(idx_wm)*100,1))])

% shared + uncertainty

idx_uncert = find(SHARE+UNCERT==1);
idx_uncert_TU = find(SHARE+UNCERT & BSR_img_unthresh);
disp(['percentage uncertainty:', num2str(round(numel(idx_uncert_TU)/numel(idx_uncert)*100,1))])

% shared + wm

idx_wm = find(SHARE+WM==1);
idx_wm_TU = find(SHARE+WM & BSR_img_unthresh);
disp(['percentage wm:', num2str(round(numel(idx_wm_TU)/numel(idx_wm)*100,1))])

%% get overlap (% of voxels in behavPLS)

% % of shared
% 
% idx_share = find(SHARE==1);
% idx_TU = find(logical(BSR_img_unthresh));
% idx_share_TU = find(SHARE & BSR_img_unthresh);
% disp(['percentage shared:', num2str(round(numel(idx_share_TU)/numel(idx_TU)*100,1))])
% 
% % of unique uncertainty
% 
% idx_uncert = find(UNCERT==1);
% idx_uncert_TU = find(UNCERT & BSR_img_unthresh);
% disp(['percentage uncertainty:', num2str(round(numel(idx_uncert_TU)/numel(idx_TU)*100,1))])
% 
% % of unique att
% 
% idx_wm = find(WM==1);
% idx_wm_TU = find(WM & BSR_img_unthresh);
% disp(['percentage wm:', num2str(round(numel(idx_wm_TU)/numel(idx_TU)*100,1))])

%% get overlap (% of voxels in Neurosynth maps)

% of shared

idx_share = find(SHARE==1);
idx_TU = find(Muller==1);
idx_share_TU = find(SHARE & Muller);
disp(['percentage shared:', num2str(round(numel(idx_share_TU)/numel(idx_share)*100,1))])

% of unique uncertainty

idx_uncert = find(UNCERT==1);
idx_uncert_TU = find(UNCERT & Muller);
disp(['percentage unique uncertainty:', num2str(round(numel(idx_uncert_TU)/numel(idx_uncert)*100,1))])

% of unique wm

idx_wm = find(WM==1);
idx_wm_TU = find(WM & Muller);
disp(['percentage unique wm:', num2str(round(numel(idx_wm_TU)/numel(idx_wm)*100,1))])

% shared + uncertainty

idx_uncert = find(SHARE+UNCERT==1);
idx_uncert_TU = find(SHARE+UNCERT & Muller);
disp(['percentage uncertainty:', num2str(round(numel(idx_uncert_TU)/numel(idx_uncert)*100,1))])

% shared + wm

idx_wm = find(SHARE+WM==1);
idx_wm_TU = find(SHARE+WM & Muller);
disp(['percentage wm:', num2str(round(numel(idx_wm_TU)/numel(idx_wm)*100,1))])


%% get overlap (% of voxels in behavPLS)

% % of shared
% 
% idx_share = find(SHARE==1);
% idx_TU = find(logical(Muller));
% idx_share_TU = find(SHARE & Muller);
% disp(['percentage shared:', num2str(round(numel(idx_share_TU)/numel(idx_TU)*100,1))])
% 
% % of unique uncertainty
% 
% idx_uncert = find(UNCERT==1);
% idx_uncert_TU = find(UNCERT & Muller);
% disp(['percentage unique uncertainty:', num2str(round(numel(idx_uncert_TU)/numel(idx_TU)*100,1))])
% 
% % of unique att
% 
% idx_wm = find(WM==1);
% idx_wm_TU = find(WM & Muller);
% disp(['percentage unique wm:', num2str(round(numel(idx_wm_TU)/numel(idx_TU)*100,1))])
% 
