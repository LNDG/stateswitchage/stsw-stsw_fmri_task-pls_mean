#!/bin/bash

module load fsl

rootpath="$(pwd)/../.."
rootpath=$(builtin cd $rootpath; pwd)

IMG1="${rootpath}/data/references/uncertainty_uniformity-test_z_FDR_0.01_binary"
IMG2="${rootpath}/data/references/workingmemory_uniformity-test_z_FDR_0.01_binary"
OUT="${rootpath}/data/references/wm_uncertainty"

# create intersection (shared activation between WM and uncertainty)

fslmaths ${IMG1}.nii.gz -mul ${IMG2}.nii.gz ${OUT}_intersect.nii
fslmaths ${OUT}_intersect.nii -thr 0.5 -bin ${OUT}_binary.nii

# create subtracting images (unique loadings for WM or uncertainty)

fslmaths ${IMG1}.nii.gz -sub ${OUT}_binary.nii ${IMG1}_wm_unique.nii
fslmaths ${IMG2}.nii.gz -sub ${OUT}_binary.nii ${IMG2}_unique.nii

# do the same for the attention map

IMG1="${rootpath}/data/references/uncertainty_uniformity-test_z_FDR_0.01_binary"
IMG2="${rootpath}/data/references/attention_uniformity-test_z_FDR_0.01_binary"
OUT="${rootpath}/data/references/attention_uncertainty"

# create intersection (shared activation between attention and uncertainty)

fslmaths ${IMG1}.nii.gz -mul ${IMG2}.nii.gz ${OUT}_intersect.nii
fslmaths ${OUT}_intersect.nii -thr 0.5 -bin ${OUT}_binary.nii

# create subtracting images (unique loadings for attention or uncertainty)

fslmaths ${IMG1}.nii.gz -sub ${OUT}_binary.nii ${IMG1}_att_unique.nii
fslmaths ${IMG2}.nii.gz -sub ${OUT}_binary.nii ${IMG2}_unique.nii