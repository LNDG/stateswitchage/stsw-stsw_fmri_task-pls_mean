
rootpath="$(pwd)/../.."
rootpath=$(builtin cd $rootpath; pwd)

# plot intersection masks

fsleyes render \
  --outfile ${rootpath}/figures/S13/neurosynth.png \
  --size 800 600 \
  -sortho -std1mm \
  --autoDisplay \
  --neuroOrientation \
  --bgColour 1 1 1 \
  --highDpi \
  --hideCursor \
  --worldLoc -4 -17 6  \
  --showGridLines \
  ${rootpath}/data/references/wm_uncertainty_binary.nii.gz \
  -cm blue \
  ${rootpath}/data/references/workingmemory_uniformity-test_z_FDR_0.01_binary_unique.nii.gz \
  -cm green \
  ${rootpath}/data/references/uncertainty_uniformity-test_z_FDR_0.01_binary_unique.nii.gz \
  -cm red