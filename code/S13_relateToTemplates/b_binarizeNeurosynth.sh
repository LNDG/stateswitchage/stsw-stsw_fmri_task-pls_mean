#!/bin/bash

module load fsl

rootpath="$(pwd)/../.."
rootpath=$(builtin cd $rootpath; pwd)

IMG1="${rootpath}/data/references/uncertainty_uniformity-test_z_FDR_0.01"
IMG2="${rootpath}/data/references/workingmemory_uniformity-test_z_FDR_0.01"
IMG3="${rootpath}/data/references/attention_uniformity-test_z_FDR_0.01"

fslmaths ${IMG1}.nii -thr 1 -bin ${IMG1}_binary.nii
fslmaths ${IMG2}.nii -thr 1 -bin ${IMG2}_binary.nii
fslmaths ${IMG3}.nii -thr 1 -bin ${IMG3}_binary.nii
