#!/bin/bash

module load fsl

rootpath="$(pwd)/../.."
rootpath=$(builtin cd $rootpath; pwd)

BASE="${rootpath}/data/references/behavPLS_STSWD_SPM_YA_OA_red_1group_3mm_1000P1000B_BfMRIbsr_lv1_unthresholded"
REF="${rootpath}/data/references/anatomical.nii"

fslchfiletype NIFTI ${BASE}.hdr
flirt -in ${BASE}.nii -ref ${REF} -applyxfm -usesqform -out ${BASE}_thr_mask.nii.gz

# repeat for task PLS

BASE="${rootpath}/data/references/taskPLS_STSWD_SPM_YA_OA_3mm_1000P1000B_BfMRIbsr_lv1"
fslchfiletype NIFTI ${BASE}.hdr
flirt -in ${BASE}.nii -ref ${REF} -applyxfm -usesqform -out ${BASE}_thr_mask.nii.gz

BASE="${rootpath}/data/references/taskPLS_STSWD_SPM_YA_OA_3mm_1000P1000B_BfMRIbsr_lv2"
fslchfiletype NIFTI ${BASE}.hdr
flirt -in ${BASE}.nii -ref ${REF} -applyxfm -usesqform -out ${BASE}_thr_mask.nii.gz

BASE="${rootpath}/data/references/taskPLS_STSWD_SPM_YA_OA_3mm_1000P1000B_BfMRIbsr_lv3"
fslchfiletype NIFTI ${BASE}.hdr
flirt -in ${BASE}.nii -ref ${REF} -applyxfm -usesqform -out ${BASE}_thr_mask.nii.gz