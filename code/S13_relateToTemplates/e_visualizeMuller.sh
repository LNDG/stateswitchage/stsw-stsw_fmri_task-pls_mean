
rootpath="$(pwd)/../.."
rootpath=$(builtin cd $rootpath; pwd)

# plot Muller (positive)

fsleyes render \
  --outfile ${rootpath}/figures/S13/Muller_pos.png \
  --size 800 600 \
  --scene ortho \
  --autoDisplay \
  --neuroOrientation \
  --bgColour 1 1 1 \
  --hideCursor \
  --highDpi \
  --showColourBar -cbl bottom \
  --worldLoc 6 -25 6  \
  --showGridLines \
  ${rootpath}/data/standards/data/mni_icbm152_nlin_sym_09c/mni_icbm152_t1_tal_nlin_sym_09c_brain.nii.gz \
  ${rootpath}/data/references/Fig1_supp3a.nii \
  -cm cbrew_pos \
  -cr 0 6 \
  -dr 0 6

  
# plot Muller (negative)

fsleyes render \
  --outfile ${rootpath}/figures/S13/Muller_neg.png \
  --size 800 600 \
  --scene ortho \
  --autoDisplay \
  --neuroOrientation \
  --bgColour 1 1 1 \
  --hideCursor \
  --highDpi \
  --showColourBar -cbl bottom \
  --worldLoc 6 -25 6  \
  ${rootpath}/data/standards/data/mni_icbm152_nlin_sym_09c/mni_icbm152_t1_tal_nlin_sym_09c_brain.nii.gz \
  ${rootpath}/data/references/Fig1_supp3a.nii \
  -cm cbrew_neg_inv \
  -cr -6 0 \
  -dr -6 0 \
  -i