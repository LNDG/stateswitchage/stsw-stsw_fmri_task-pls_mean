function S2_make_datamat(id, rootpath)

% Create the mean datamats based on the info mats.

if ismac % run if function is not pre-compiled
    id = '1126'; % test for example subject
    currentFile = mfilename('fullpath');
    [pathstr,~,~] = fileparts(currentFile);
    cd(fullfile(pathstr,'..', '..'))
    rootpath = pwd;
end

pn.plstoolbox = fullfile(rootpath, 'tools', 'pls');
addpath(genpath(pn.plstoolbox));

pn.PLSfiles = fullfile(rootpath, 'data', 'pls');
cd(pn.PLSfiles); % files will be output in the CD directory

fprintf('Creating PLS_data_mat for subject %s \n', id);
BATCHPATH = fullfile(pn.PLSfiles, [id, '_task_PLS_info.mat']);
batch_plsgui(BATCHPATH);
fprintf('Finished creating PLS_data_mat for subject %s \n', id);

end