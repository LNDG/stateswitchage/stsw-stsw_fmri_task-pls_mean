% Create info file for PLS analysis

% for ER-behavioral PLS: include AMF, relevant DDM parameters

clear all; clc;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..', '..'))
rootpath = pwd;

pn.PLSfiles = fullfile(rootpath, 'data', 'pls');
pn.summary = fullfile(rootpath, '..', '..', 'stsw_multimodal', 'data');


%% IDs

filename = fullfile(rootpath, 'code', 'id_list_eegmr.txt');
fileID = fopen(filename);
IDs_EEGMR = textscan(fileID,'%s');
fclose(fileID);
IDs_EEGMR = IDs_EEGMR{1};

IDs_YA = IDs_EEGMR(cellfun(@str2num, IDs_EEGMR)<2000);
IDs_OA = IDs_EEGMR(cellfun(@str2num, IDs_EEGMR)>2000);

%% get summary data

load(fullfile(pn.summary, 'STSWD_summary_YAOA.mat'), 'STSWD_summary')

%% YA

% select subjects (multimodal only)
idxMulti_summary = ismember(STSWD_summary.IDs, IDs_YA);
[STSWD_summary.IDs(idxMulti_summary), IDs_YA]

numSubs = numel(find(ismember(STSWD_summary.IDs,IDs_YA)));
IDs = IDs_YA(ismember(IDs_YA,STSWD_summary.IDs));
dataForPLS = [];
for indCond = 1
    for indID = 1:numel(IDs)
        disp(['Processing ID ', IDs{indID}])
        groupfiles{indID,1} = ['task_',IDs{indID}, '_BfMRIsessiondata.mat'];
        curID = find(strcmp(STSWD_summary.IDs, IDs{indID}));
        dataForPLS{(indCond-1)*numSubs+indID,1} = STSWD_summary.HDDM_vat.driftEEGMRI_linear_win(curID);
        dataForPLS{(indCond-1)*numSubs+indID,2} = STSWD_summary.pupil_LV1.linear_win(curID);
        dataForPLS{(indCond-1)*numSubs+indID,3} = STSWD_summary.tfr_theta_LV1.linear_win(curID);
        dataForPLS{(indCond-1)*numSubs+indID,4} = STSWD_summary.tfr_alpha_LV1.linear_win(curID);
        dataForPLS{(indCond-1)*numSubs+indID,5} = STSWD_summary.fooof_LV1.linear_win(curID);
        dataForPLS{(indCond-1)*numSubs+indID,6} = STSWD_summary.SE_LV1.linear_win(curID);
        dataname{(indCond-1)*numSubs+indID,1} = 'behavior_data';
    end
end
groupfiles = groupfiles(find(~cellfun(@isempty,groupfiles)));

% normalize each variable: convert to z-scores
%dataForPLS_z = [dataForPLS(:,1), num2cell(zscore(cell2mat(dataForPLS(:,2:end))))];

Fillin.GROUPFILES = groupfiles';
Fillin.DATANAME = dataname;
Fillin.VALUE = dataForPLS;

% Output necessary information, has to be manually copied into .txt file

clc;
for indSub = 1:numel(IDs) 
    fprintf('%s ', Fillin.GROUPFILES{indSub});
end

clc;
for indSub = 1:size(Fillin.VALUE,1) 
    fprintf('%s    %d %d %d %d %d %d \n', Fillin.DATANAME{indSub}, Fillin.VALUE{indSub,:});
end

%% OA

% select subjects (multimodal only)
idxMulti_summary = ismember(STSWD_summary.IDs, IDs_OA);
[STSWD_summary.IDs(idxMulti_summary), IDs_OA]

numSubs = numel(find(ismember(STSWD_summary.IDs,IDs_OA)));
IDs = IDs_OA(ismember(IDs_OA,STSWD_summary.IDs));
dataForPLS = [];
for indCond = 1
    for indID = 1:numel(IDs)
        disp(['Processing ID ', IDs{indID}])
        groupfiles{indID,1} = ['task_',IDs{indID}, '_BfMRIsessiondata.mat'];
        curID = find(strcmp(STSWD_summary.IDs, IDs{indID}));
        dataForPLS{(indCond-1)*numSubs+indID,1} = STSWD_summary.HDDM_vat.driftEEGMRI_linear_win(curID);
        dataForPLS{(indCond-1)*numSubs+indID,2} = STSWD_summary.pupil_LV1.linear_win(curID);
        dataForPLS{(indCond-1)*numSubs+indID,3} = STSWD_summary.tfr_theta_LV1.linear_win(curID);
        dataForPLS{(indCond-1)*numSubs+indID,4} = STSWD_summary.tfr_alpha_LV1.linear_win(curID);
        dataForPLS{(indCond-1)*numSubs+indID,5} = STSWD_summary.fooof_LV1.linear_win(curID);
        dataForPLS{(indCond-1)*numSubs+indID,6} = STSWD_summary.SE_LV1.linear_win(curID);
        dataname{(indCond-1)*numSubs+indID,1} = 'behavior_data';
    end
end
groupfiles = groupfiles(find(~cellfun(@isempty,groupfiles)));

% normalize each variable: convert to z-scores
%dataForPLS_z = [dataForPLS(:,1), num2cell(zscore(cell2mat(dataForPLS(:,2:end))))];

Fillin.GROUPFILES = groupfiles';
Fillin.DATANAME = dataname;
Fillin.VALUE = dataForPLS;

% Output necessary information, has to be manually copied into .txt file

clc;
for indSub = 1:numel(IDs) 
    fprintf('%s ', Fillin.GROUPFILES{indSub});
end

clc;
for indSub = 1:size(Fillin.VALUE,1) 
    fprintf('%s    %d %d %d %d %d %d \n', Fillin.DATANAME{indSub}, Fillin.VALUE{indSub,:});
end

behavior_name ={'driftchange', 'pupil', 'theta', ...
    'alpha', 'fooof', 'sampen'};

clc;
fprintf('%s    %s %s %s %s %s %s \n', 'behavior_name', behavior_name{:});


% figure;imagesc(cell2mat(dataForPLS_z(:,2:end)))
% figure;imagesc(corrcoef(cell2mat(dataForPLS_z(:,2:end))))
% 
% [lv, coeff] = pca(cell2mat(dataForPLS_z(:,2:end)));
% figure; imagesc(lv)


