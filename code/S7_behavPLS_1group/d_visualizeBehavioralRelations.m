
currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..', '..'))
rootpath = pwd;

pn.data = fullfile(rootpath, 'data', 'pls');
pn.figures = fullfile(rootpath, 'figures', 'behavPLS_1group'); mkdir(pn.figures)
pn.summary = fullfile(rootpath, '..', '..', 'stsw_multimodal', 'data');
pn.tools = fullfile(rootpath, 'tools'); 
    addpath(fullfile(pn.tools));
    addpath(fullfile(pn.tools, 'barwitherr'));

colorm = [230/265 25/265 25/265; 0/265 50/265 100/265];

load(fullfile(pn.data, 'behavPLS_STSWD_SPM_YA_OA_red_1group_3mm_1000P1000B_BfMRIresult.mat'))

filename = fullfile(rootpath, 'code', 'id_list_eegmr.txt');
fileID = fopen(filename);
IDs_EEGMR = textscan(fileID,'%s');
fclose(fileID);
IDs_EEGMR = IDs_EEGMR{1};

IDs_YA = IDs_EEGMR(cellfun(@str2num, IDs_EEGMR)<2000);
IDs_OA = IDs_EEGMR(cellfun(@str2num, IDs_EEGMR)>2000);

groupsizes=result.num_subj_lst;
conditions=lv_evt_list;

conds = {'linear1234'};

condData = []; uData = []; u2Data = [];
for indGroup = 1:1
    if indGroup == 1
        relevantEntries = 1:groupsizes(1)*numel(conds);
    elseif indGroup == 2
        relevantEntries = groupsizes(1)*numel(conds)+1:...
             groupsizes(1)*numel(conds)+groupsizes(2)*numel(conds);
    end
    for indCond = 1:numel(conds)
        targetEntries = relevantEntries(conditions(relevantEntries)==indCond);        
        condData{indGroup}(indCond,:) = result.vsc(targetEntries,1);
        uData{indGroup}(indCond,:) = result.usc(targetEntries,1);
    end
end

correlationCI = [result.boot_result.llcorr(:,1),result.boot_result.ulcorr(:,1)];
correlation = result.boot_result.orig_corr(:,1);
correlationCI = abs(correlationCI-correlation);

individualBrainScores = uData{1}';
individualBehavScores = condData{1}';

%% save individual brainscores & lin. modulation

LV1.BrainS = uData{1}';
LV1.BehavS = condData{1}';
LV1.IDs = IDs_EEGMR;

save(fullfile(pn.data, 'behavPLS_LV1.mat'), 'LV1')

behavioralsigs = {'drift', 'pupil', 'theta', 'alpha', 'fooof', 'sampen'};

%% load summary data

idxYA = find(str2double(IDs_EEGMR)<2000);
idxOA = find(str2double(IDs_EEGMR)>=2000);

load(fullfile(pn.summary, 'STSWD_summary_YAOA.mat'), 'STSWD_summary')

idx_sumID = ismember(STSWD_summary.IDs, IDs_EEGMR);

%% plot barplot

h = figure('units','centimeters','position',[5 5 10 8]);
set(0, 'DefaultFigureRenderer', 'painters');
[hBar hErrorbar] = barwitherr(correlationCI, correlation)
set(hBar, 'FaceColor', [.6 .8 .8], 'EdgeColor', 'none')
set(hErrorbar, 'LineWidth', 1.5)
ylabel('Correlation')
xticklabels(behavioralsigs)
xtickangle(90)
set(findall(gcf,'-property','FontSize'),'FontSize',17)
figureName = ['d_correlations_1group_behavpls'];
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

%% plot barplot with partial correlations

fields_name = {'driftEEGMRI', ...
   'thresholdEEGMRI', ...
   'nondecisionEEGMRI', ...
   'driftEEGMRI_linear_win', ...
   'thresholdEEGMRI_linear_win', ...
   'nondecisionEEGMRI_linear_win', ...
   'tfr_theta_LV1',...
   'tfr_prestim_alpha_LV1', ...
   'pupil_LV1', ...
   'CPPslopes', ...
   'CPPthreshold', ...
   'SSVEPmag_norm', ...
   'tfr_alpha_LV1', ...
   'SE_LV1',...
   'fooof_LV1'};

fields_title = {...
   'drift-l1',...
   'threshold-l1', ...
   'ndt-l1', ...
   'drift',...
   'threshold', ...
   'ndt', ...
   'theta',...
   'prestimalpha', ...
   'pupil', ...
   'cpp', ...
   'cppthresh', ...
   'ssvep', ...
   'alpha', ...
   'sampen',...
   'fooof'};

correlations = []; correlations_CI = [];
partialcorrelations = []; partialcorrelations_CI = [];
for indParam = 1:numel(fields_name)
    x = individualBrainScores;
    if isfield(STSWD_summary, (fields_name{indParam}))
        y = STSWD_summary.(fields_name{indParam}).linear_win(idx_sumID)';
    else
        tmp = STSWD_summary.HDDM_vat.(fields_name{indParam});
        if size(tmp,2)>size(tmp,1)
            tmp = tmp';
        end
        y = tmp(idx_sumID,1);
    end
    [r, p, RL, RU] = corrcoef(x, y);
    
    correlations(indParam,1) = r(2);
    correlations_CI(indParam,1) = RL(2);
    correlations_CI(indParam,2) = RU(2);

    X = [repmat(1,numel(x),1), cat(1, repmat(1,numel(find(idxYA)),1), repmat(2,numel(find(idxOA)),1))];
    [b, tmp, x_r] = regress(x,X);
    [b, tmp, y_r] = regress(y,X);
    [r, p, RL, RU] = corrcoef(x_r, y_r);
    
    partialcorrelations(indParam,1) = r(2);
    partialcorrelations_CI(indParam,1) = RL(2);
    partialcorrelations_CI(indParam,2) = RU(2);
end

correlations_CI = abs(correlations_CI-correlations);
partialcorrelations_CI = abs(partialcorrelations_CI-partialcorrelations);

h = figure('units','centimeters','position',[5 5 10 8]);
set(0, 'DefaultFigureRenderer', 'painters');
[~, sortidx] = sort(correlations, 'descend');
[hBar hErrorbar] = barwitherr(correlations_CI(sortidx,:), correlations(sortidx))
set(hBar, 'FaceColor', [.6 .8 .8], 'EdgeColor', 'none')
set(hErrorbar, 'LineWidth', 1.5)
ylabel('Correlation')
xticklabels(fields_title(sortidx))
xtickangle(90)
set(findall(gcf,'-property','FontSize'),'FontSize',12)
figureName = ['d_correlations_1group_behavpls_extended'];
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

h = figure('units','centimeters','position',[5 5 10 8]);
set(0, 'DefaultFigureRenderer', 'painters');
[hBar hErrorbar] = barwitherr(partialcorrelations_CI(sortidx,:), partialcorrelations(sortidx))
set(hBar, 'FaceColor', [.6 .8 .8], 'EdgeColor', 'none')
set(hErrorbar, 'LineWidth', 1.5)
ylabel('Correlation')
xticklabels(fields_title(sortidx))
xtickangle(90)
title('Age-partialed')
set(findall(gcf,'-property','FontSize'),'FontSize',12)
figureName = ['d_correlations_1group_behavpls_partialed'];
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

% set(findall(gcf,'-property','FontSize'),'FontSize',17)
% figureName = ['d_correlations_1group_behavpls'];
% saveas(h, fullfile(pn.figures, figureName), 'epsc');
% saveas(h, fullfile(pn.figures, figureName), 'png');


%% reproduce correlation plots
% split by YA/OA

idxYA = find(str2double(IDs_EEGMR)<2000);
idxOA = find(str2double(IDs_EEGMR)>=2000);

load(fullfile(pn.summary, 'STSWD_summary_YAOA.mat'), 'STSWD_summary')

idx_sumID = ismember(STSWD_summary.IDs, IDs_EEGMR);

fields_name = {'driftEEGMRI', ...
   'thresholdEEGMRI', ...
   'nondecisionEEGMRI', ...
   'driftEEGMRI_linear_win', ...
   'thresholdEEGMRI_linear_win', ...
   'nondecisionEEGMRI_linear_win', ...
   'tfr_theta_LV1',...
   'tfr_prestim_alpha_LV1', ...
   'pupil_LV1', ...
   'CPPslopes', ...
   'CPPthreshold', ...
   'SSVEPmag_norm', ...
   'tfr_alpha_LV1', ...
   'SE_LV1',...
   'fooof_LV1'};

fields_title = {...
   'drift-l1',...
   'threshold-l1', ...
   'ndt-l1', ...
   'drift',...
   'threshold', ...
   'ndt', ...
   'theta',...
   'prestimalpha', ...
   'pupil', ...
   'cpp', ...
   'cppthresh', ...
   'ssvep', ...
   'alpha', ...
   'sampen',...
   'fooof'};

for indParam = 1:numel(fields_name)
    h = figure('units','normalized','position',[.1 .1 .2 .13]);
    x = individualBrainScores;
    if isfield(STSWD_summary, (fields_name{indParam}))
        y = STSWD_summary.(fields_name{indParam}).linear_win(idx_sumID)';
    else
        tmp = STSWD_summary.HDDM_vat.(fields_name{indParam});
        if size(tmp,2)>size(tmp,1)
            tmp = tmp';
        end
        y = tmp(idx_sumID,1);
    end
    ax{1} = axes('Position',[0.2 0.2 0.3 0.7]); cla; hold on;
    ax{1}.PositionConstraint = 'innerposition';
        scatter(x, y, 1, 'filled', 'MarkerFaceColor', [.8 .8 .8]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
        scatter(x(idxYA), y(idxYA), 50, 'square', 'filled', 'MarkerFaceColor', .2+[.6 .8 .8]);
        scatter(x(idxOA), y(idxOA), 50, 'diamond', 'filled', 'MarkerFaceColor', [.6 .8 .8]); 
%         scatter(x(idxYA), y(idxYA), 40, 'filled', 'MarkerFaceColor', [1 .6 .6]);
%         scatter(x(idxOA), y(idxOA), 40, 'filled', 'MarkerFaceColor', [1 1 1]); 
        [r, p, RL, RU] = corrcoef(x, y);
        title(['r = ', num2str(round(r(2),2)), ' CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']'])
        ylabel({fields_title{indParam};'(linear mod.)'}); xlabel('fMRI Brainscore (arb.u.)')
        ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
        xlim([min(x)-(.1.*(max(x)-min(x))), max(x)+(.1.*(max(x)-min(x)))])
    ax{2} = axes('Position',[0.65 0.2 0.3 0.7]); cla; hold on;
    ax{2}.PositionConstraint = 'innerposition';
        X = [repmat(1,numel(x),1), cat(1, repmat(1,numel(find(idxYA)),1), repmat(2,numel(find(idxOA)),1))];
        [b, tmp, x_r] = regress(x,X);
        [b, tmp, y_r] = regress(y,X);
        scatter(x_r, y_r, 40, 'filled', 'MarkerFaceColor', [.8 .8 .8]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
        [r, p, RL, RU] = corrcoef(x_r, y_r);
        title(['r = ', num2str(round(r(2),2)), ' CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']'])
        ylabel({fields_title{indParam};'(linear mod.)'}); xlabel('fMRI Brainscore (arb.u.)')
        ylim([min(y_r)-(.1.*(max(y_r)-min(y_r))), max(y_r)+(.1.*(max(y_r)-min(y_r)))])
        xlim([min(x_r)-(.1.*(max(x_r)-min(x_r))), max(x_r)+(.1.*(max(x_r)-min(x_r)))])
	set(h,'Color','w')
    for indAx = 1:2; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',14)
    h.InvertHardcopy = 'off';
    figureName = ['d_correlations_', fields_title{indParam}];
    saveas(h, fullfile(pn.figures, figureName), 'epsc');
    saveas(h, fullfile(pn.figures, figureName), 'png');
end

%% plot behavioral vs brainscore plot

h = figure('units','normalized','position',[.1 .1 .2 .13]);
x = individualBrainScores;
ax{1} = subplot(1,2,1); cla; hold on;
    y = individualBehavScores;
    scatter(x, y, 1, 'filled', 'MarkerFaceColor', [.8 .8 .8]); l1 = lsline(); 
    set(l1, 'Color',[.6 .8 .8], 'LineWidth', 3);
    scatter(x(idxYA), y(idxYA), 50, 'square', 'filled', 'MarkerFaceColor', .2+[.6 .8 .8]);
    scatter(x(idxOA), y(idxOA), 50, 'diamond', 'filled', 'MarkerFaceColor', [.6 .8 .8]); 
    [r, p, RL, RU] = corrcoef(x, y);
    title(['r = ', num2str(round(r(2),2)), ' CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']'])
    ylabel('Behavioral Score'); xlabel('fMRI Brainscore (arb.u.)')
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    xlim([min(x)-(.1.*(max(x)-min(x))), max(x)+(.1.*(max(x)-min(x)))])
ax{2} = subplot(1,2,2); cla; hold on;
    X = [repmat(1,numel(x),1), cat(1, repmat(1,numel(find(idxYA)),1), repmat(2,numel(find(idxOA)),1))];
    y = individualBehavScores;
    [b, tmp, x_r] = regress(x,X);
    [b, tmp, y_r] = regress(y,X);
    scatter(x_r, y_r, 40, 'filled', 'MarkerFaceColor', [.8 .8 .8]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
    [r, p, RL, RU] = corrcoef(x_r, y_r);
    title(['r = ', num2str(round(r(2),2)), ' CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']'])
    ylabel('Behavioral Score'); xlabel('fMRI Brainscore (arb.u.)')
    ylim([min(y_r)-(.1.*(max(y_r)-min(y_r))), max(y_r)+(.1.*(max(y_r)-min(y_r)))])
    xlim([min(x_r)-(.1.*(max(x_r)-min(x_r))), max(x_r)+(.1.*(max(x_r)-min(x_r)))])
set(h,'Color','w')
for indAx = 1:2; ax{indAx}.Color = [.2 .2 .2]; end
set(findall(gcf,'-property','FontSize'),'FontSize',14)
h.InvertHardcopy = 'off';
figureName = ['d_correlations_behavscore'];
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

%% same for Spearman

h = figure('units','normalized','position',[.1 .1 .2 .13]);
x = individualBrainScores;
ax{1} = subplot(1,2,1); cla; hold on;
    y = individualBehavScores;
    scatter(x, y, 1, 'filled', 'MarkerFaceColor', [.8 .8 .8]); l1 = lsline(); 
    set(l1, 'Color',[.6 .8 .8], 'LineWidth', 3);
    scatter(x(idxYA), y(idxYA), 50, 'square', 'filled', 'MarkerFaceColor', .2+[.6 .8 .8]);
    scatter(x(idxOA), y(idxOA), 50, 'diamond', 'filled', 'MarkerFaceColor', [.6 .8 .8]); 
    [r, p] = corr(x, y, 'type', 'Spearman');
    title(['r = ', num2str(round(r,2)), ', p = ', num2str(round(p,10))])
    ylabel('Behavioral Score'); xlabel('fMRI Brainscore (arb.u.)')
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    xlim([min(x)-(.1.*(max(x)-min(x))), max(x)+(.1.*(max(x)-min(x)))])
ax{2} = subplot(1,2,2); cla; hold on;
    X = [repmat(1,numel(x),1), cat(1, repmat(1,numel(find(idxYA)),1), repmat(2,numel(find(idxOA)),1))];
    y = individualBehavScores;
    [b, tmp, x_r] = regress(x,X);
    [b, tmp, y_r] = regress(y,X);
    scatter(x_r, y_r, 40, 'filled', 'MarkerFaceColor', [.8 .8 .8]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
    [r, p] = corr(x_r, y_r, 'type', 'Spearman');
    title(['r = ', num2str(round(r,2)), ', p = ', num2str(round(p,10))])
    ylabel('Behavioral Score'); xlabel('fMRI Brainscore (arb.u.)')
    ylim([min(y_r)-(.1.*(max(y_r)-min(y_r))), max(y_r)+(.1.*(max(y_r)-min(y_r)))])
    xlim([min(x_r)-(.1.*(max(x_r)-min(x_r))), max(x_r)+(.1.*(max(x_r)-min(x_r)))])
set(h,'Color','w')
for indAx = 1:2; ax{indAx}.Color = [.2 .2 .2]; end
set(findall(gcf,'-property','FontSize'),'FontSize',14)
h.InvertHardcopy = 'off';

%% correlate with relative drift rate changes


idxYA = find(str2double(IDs_EEGMR)<2000);
idxOA = find(str2double(IDs_EEGMR)>=2000);

load(fullfile(pn.summary, 'STSWD_summary_YAOA.mat'), 'STSWD_summary')

idx_sumID = ismember(STSWD_summary.IDs, IDs_EEGMR);

fields_name = {'driftEEGMRI', ...
   'driftEEGMRI_linear_win', ...
   'driftEEGMRI_pc_linear_win'};

fields_title = {...
   'drift-l1', ...
   'drift', ...
   'drift pc'};

for indParam = 1:numel(fields_name)
    h = figure('units','normalized','position',[.1 .1 .2 .13]);
    x = individualBrainScores;
    if isfield(STSWD_summary, (fields_name{indParam}))
        y = STSWD_summary.(fields_name{indParam}).linear_win(idx_sumID)';
    else
        tmp = STSWD_summary.HDDM_vat.(fields_name{indParam});
        if size(tmp,2)>size(tmp,1)
            tmp = tmp';
        end
        if indParam==3
            y = squeeze(nanmean(tmp(idx_sumID,2:4),2));
        else
            y = tmp(idx_sumID,1);
        end
    end
    ax{1} = axes('Position',[0.2 0.2 0.3 0.7]); cla; hold on;
%     ax{1}.PositionConstraint = 'innerposition';
        scatter(x, y, 1, 'filled', 'MarkerFaceColor', [.8 .8 .8]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
        scatter(x(idxYA), y(idxYA), 50, 'square', 'filled', 'MarkerFaceColor', .2+[.6 .8 .8]);
        scatter(x(idxOA), y(idxOA), 50, 'diamond', 'filled', 'MarkerFaceColor', [.6 .8 .8]); 
%         scatter(x(idxYA), y(idxYA), 40, 'filled', 'MarkerFaceColor', [1 .6 .6]);
%         scatter(x(idxOA), y(idxOA), 40, 'filled', 'MarkerFaceColor', [1 1 1]); 
        [r, p, RL, RU] = corrcoef(x, y);
        title(['r = ', num2str(round(r(2),2)), ' CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']'])
        ylabel({fields_title{indParam};'(linear mod.)'}); xlabel('fMRI Brainscore (arb.u.)')
        ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
        xlim([min(x)-(.1.*(max(x)-min(x))), max(x)+(.1.*(max(x)-min(x)))])
    ax{2} = axes('Position',[0.65 0.2 0.3 0.7]); cla; hold on;
%     ax{2}.PositionConstraint = 'innerposition';
        X = [repmat(1,numel(x),1), cat(1, repmat(1,numel(find(idxYA)),1), repmat(2,numel(find(idxOA)),1))];
        [b, tmp, x_r] = regress(x,X);
        [b, tmp, y_r] = regress(y,X);
        scatter(x_r, y_r, 40, 'filled', 'MarkerFaceColor', [.8 .8 .8]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
        [r, p, RL, RU] = corrcoef(x_r, y_r);
        title(['r = ', num2str(round(r(2),2)), ' CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']'])
        ylabel({fields_title{indParam};'(linear mod.)'}); xlabel('fMRI Brainscore (arb.u.)')
        ylim([min(y_r)-(.1.*(max(y_r)-min(y_r))), max(y_r)+(.1.*(max(y_r)-min(y_r)))])
        xlim([min(x_r)-(.1.*(max(x_r)-min(x_r))), max(x_r)+(.1.*(max(x_r)-min(x_r)))])
	set(h,'Color','w')
    for indAx = 1:2; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',14)
    h.InvertHardcopy = 'off';
    figureName = ['d_correlations_', fields_title{indParam}];
%     saveas(h, fullfile(pn.figures, figureName), 'epsc');
%     saveas(h, fullfile(pn.figures, figureName), 'png');
end

%% for matched data

idxYA = find(str2double(IDs_EEGMR)<2000);
idxOA = find(str2double(IDs_EEGMR)>=2000);

load(fullfile(pn.summary, 'STSWD_summary_YAOA.mat'), 'STSWD_summary')

idx_sumID = ismember(STSWD_summary.IDs, IDs_EEGMR);

fields_name = {'driftEEGMRI', ...
   'driftEEGMRI_linear_win', ...
   'driftEEGMRI_pc'};

fields_title = {...
   'drift-l1', ...
   'drift', ...
   'drift pc'};

for indParam = 1:numel(fields_name)
    h = figure('units','normalized','position',[.1 .1 .2 .13]);
    x = individualBrainScores;
    if isfield(STSWD_summary, (fields_name{indParam}))
        y = STSWD_summary.(fields_name{indParam}).linear_win(idx_sumID)';
    else
        tmp = STSWD_summary.HDDM_vat_matched.(fields_name{indParam});
        if size(tmp,2)>size(tmp,1)
            tmp = tmp';
        end
        if indParam==3
            y = squeeze(nanmean(tmp(idx_sumID,2:4),2));
        else
            y = tmp(idx_sumID,1);
        end
    end
    ax{1} = axes('Position',[0.2 0.2 0.3 0.7]); cla; hold on;
%     ax{1}.PositionConstraint = 'innerposition';
        scatter(x, y, 1, 'filled', 'MarkerFaceColor', [.8 .8 .8]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
        scatter(x(idxYA), y(idxYA), 50, 'square', 'filled', 'MarkerFaceColor', .2+[.6 .8 .8]);
        scatter(x(idxOA), y(idxOA), 50, 'diamond', 'filled', 'MarkerFaceColor', [.6 .8 .8]); 
%         scatter(x(idxYA), y(idxYA), 40, 'filled', 'MarkerFaceColor', [1 .6 .6]);
%         scatter(x(idxOA), y(idxOA), 40, 'filled', 'MarkerFaceColor', [1 1 1]); 
        [r, p, RL, RU] = corrcoef(x, y);
        title(['r = ', num2str(round(r(2),2)), ' CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']'])
        ylabel({fields_title{indParam};'(linear mod.)'}); xlabel('fMRI Brainscore (arb.u.)')
        ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
        xlim([min(x)-(.1.*(max(x)-min(x))), max(x)+(.1.*(max(x)-min(x)))])
    ax{2} = axes('Position',[0.65 0.2 0.3 0.7]); cla; hold on;
%     ax{2}.PositionConstraint = 'innerposition';
        X = [repmat(1,numel(x),1), cat(1, repmat(1,numel(find(idxYA)),1), repmat(2,numel(find(idxOA)),1))];
        [b, tmp, x_r] = regress(x,X);
        [b, tmp, y_r] = regress(y,X);
        scatter(x_r, y_r, 40, 'filled', 'MarkerFaceColor', [.8 .8 .8]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
        [r, p, RL, RU] = corrcoef(x_r, y_r);
        title(['r = ', num2str(round(r(2),2)), ' CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']'])
        ylabel({fields_title{indParam};'(linear mod.)'}); xlabel('fMRI Brainscore (arb.u.)')
        ylim([min(y_r)-(.1.*(max(y_r)-min(y_r))), max(y_r)+(.1.*(max(y_r)-min(y_r)))])
        xlim([min(x_r)-(.1.*(max(x_r)-min(x_r))), max(x_r)+(.1.*(max(x_r)-min(x_r)))])
	set(h,'Color','w')
    for indAx = 1:2; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',14)
    h.InvertHardcopy = 'off';
    figureName = ['d_correlations_', fields_title{indParam}];
%     saveas(h, fullfile(pn.figures, figureName), 'epsc');
%     saveas(h, fullfile(pn.figures, figureName), 'png');
end