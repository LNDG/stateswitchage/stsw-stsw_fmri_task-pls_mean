
currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..', '..'))
rootpath = pwd;

pn.data = fullfile(rootpath, 'data', 'pls');
pn.summary = fullfile(rootpath, '..', '..', 'stsw_multimodal', 'data');
pn.tools = fullfile(rootpath, 'tools'); 
    addpath(fullfile(pn.tools));
    addpath(fullfile(pn.tools, 'barwitherr'));

%% load data

load(fullfile(pn.summary, 'STSWDsummary_YAOA_linEffects_forR.mat'),'data')

dataS = cell2table(data(2:end,:));
dataS.Properties.VariableNames = data(1,:);
dataS.age = categorical(dataS.age);

% delete any subjects with missing data (in this case no MR)
dataS=dataS(~any(ismissing(dataS),2),:);

%% first assess whether there is a significant group*signature interaction

% behavioral score

lme = fitlme(dataS,'behav_brainscore~behav_behavscore*age');
lme

% driftchange

lme = fitlme(dataS,'behav_brainscore~drift*age');
lme

% pupil

lme = fitlme(dataS,'behav_brainscore~pupil*age');
lme

% theta

lme = fitlme(dataS,'behav_brainscore~theta*age');
lme

% alpha

lme = fitlme(dataS,'behav_brainscore~alpha*age');
lme

% fooof

lme = fitlme(dataS,'behav_brainscore~fooof*age');
lme

% sampen

lme = fitlme(dataS,'behav_brainscore~sampen*age');
lme

% excitability

lme = fitlme(dataS,'behav_brainscore~excitability*age');
lme

%% if there is no significant interaction, report only main effects

% report partial eta square
% https://de.mathworks.com/matlabcentral/answers/476748-calculate-partial-eta-squared-from-fitlme

% behavioral score

lme = fitlme(dataS,'behav_brainscore~behav_behavscore+age');
lme
% estimate effect size
[beta, ~, stats] = fixedEffects(lme);
y = dataS.behav_brainscore;
x1 = dataS.age;
x2 = dataS.behav_behavscore;
y_est= [double(x1) x2]*beta(2:3);
y_x2 = [x2]*beta(3);
y_x1 = [double(x1)]*beta(2);
residual = y - y_est;
Eta2=var(y_est)/var(y) % total effect size
Eta2_x2=var(y_x2)/(var(y_x2)+var(residual)) % partial effect size for effect of x2 on y
Eta2_x1=var(y_x1)/(var(y_x1)+var(residual)) % partial effect size for effect of x1 on y
CI = round([stats.Lower(3), stats.Upper(3)])
CI = round([stats.Lower(2), stats.Upper(2)])

% driftchange

lme = fitlme(dataS,'behav_brainscore~drift+age');
lme
% estimate effect size
[beta, ~, stats] = fixedEffects(lme); 
y = dataS.behav_brainscore;
x1 = dataS.age;
x2 = dataS.drift;
y_est= [double(x1) x2]*beta(2:3);
y_x2 = [x2]*beta(3);
y_x1 = [double(x1)]*beta(2);
residual = y - y_est;
Eta2=var(y_est)/var(y) % total effect size
Eta2_x2=var(y_x2)/(var(y_x2)+var(residual)) % partial effect size for effect of x2 on y
Eta2_x1=var(y_x1)/(var(y_x1)+var(residual)) % partial effect size for effect of x1 on y
CI = round([stats.Lower(3), stats.Upper(3)])
CI = round([stats.Lower(2), stats.Upper(2)])

% pupil

lme = fitlme(dataS,'behav_brainscore~pupil+age');
lme
% estimate effect size
[beta, ~, stats] = fixedEffects(lme); 
y = dataS.behav_brainscore;
x1 = dataS.age;
x2 = dataS.pupil;
y_est= [double(x1) x2]*beta(2:3);
y_x2 = [x2]*beta(3);
y_x1 = [double(x1)]*beta(2);
residual = y - y_est;
Eta2=var(y_est)/var(y) % total effect size
Eta2_x2=var(y_x2)/(var(y_x2)+var(residual)) % partial effect size for effect of x2 on y
Eta2_x1=var(y_x1)/(var(y_x1)+var(residual)) % partial effect size for effect of x1 on y
CI = round([stats.Lower(3), stats.Upper(3)])
CI = round([stats.Lower(2), stats.Upper(2)])

% theta

lme = fitlme(dataS,'behav_brainscore~theta+age');
lme
% estimate effect size
[beta, ~, stats] = fixedEffects(lme); 
y = dataS.behav_brainscore;
x1 = dataS.age;
x2 = dataS.theta;
y_est= [double(x1) x2]*beta(2:3);
y_x2 = [x2]*beta(3);
y_x1 = [double(x1)]*beta(2);
residual = y - y_est;
Eta2=var(y_est)/var(y) % total effect size
Eta2_x2=var(y_x2)/(var(y_x2)+var(residual)) % partial effect size for effect of x2 on y
Eta2_x1=var(y_x1)/(var(y_x1)+var(residual)) % partial effect size for effect of x1 on y
CI = round([stats.Lower(3), stats.Upper(3)])
CI = round([stats.Lower(2), stats.Upper(2)])

% alpha

lme = fitlme(dataS,'behav_brainscore~alpha+age');
lme
% estimate effect size
[beta, ~, stats] = fixedEffects(lme); 
y = dataS.behav_brainscore;
x1 = dataS.age;
x2 = dataS.alpha;
y_est= [double(x1) x2]*beta(2:3);
y_x2 = [x2]*beta(3);
y_x1 = [double(x1)]*beta(2);
residual = y - y_est;
Eta2=var(y_est)/var(y) % total effect size
Eta2_x2=var(y_x2)/(var(y_x2)+var(residual)) % partial effect size for effect of x2 on y
Eta2_x1=var(y_x1)/(var(y_x1)+var(residual)) % partial effect size for effect of x1 on y
CI = round([stats.Lower(3), stats.Upper(3)])
CI = round([stats.Lower(2), stats.Upper(2)])

% fooof

lme = fitlme(dataS,'behav_brainscore~fooof+age');
lme
% estimate effect size
[beta, ~, stats] = fixedEffects(lme); 
y = dataS.behav_brainscore;
x1 = dataS.age;
x2 = dataS.fooof;
y_est= [double(x1) x2]*beta(2:3);
y_x2 = [x2]*beta(3);
y_x1 = [double(x1)]*beta(2);
residual = y - y_est;
Eta2=var(y_est)/var(y) % total effect size
Eta2_x2=var(y_x2)/(var(y_x2)+var(residual)) % partial effect size for effect of x2 on y
Eta2_x1=var(y_x1)/(var(y_x1)+var(residual)) % partial effect size for effect of x1 on y
CI = round([stats.Lower(3), stats.Upper(3)])
CI = round([stats.Lower(2), stats.Upper(2)])

% sampen

lme = fitlme(dataS,'behav_brainscore~sampen+age');
lme
% estimate effect size
[beta, ~, stats] = fixedEffects(lme); 
y = dataS.behav_brainscore;
x1 = dataS.age;
x2 = dataS.sampen;
y_est= [double(x1) x2]*beta(2:3);
y_x2 = [x2]*beta(3);
y_x1 = [double(x1)]*beta(2);
residual = y - y_est;
Eta2=var(y_est)/var(y) % total effect size
Eta2_x2=var(y_x2)/(var(y_x2)+var(residual)) % partial effect size for effect of x2 on y
Eta2_x1=var(y_x1)/(var(y_x1)+var(residual)) % partial effect size for effect of x1 on y
CI = round([stats.Lower(3), stats.Upper(3)])
CI = round([stats.Lower(2), stats.Upper(2)])

% excitability

% lme = fitlme(dataS,'behav_brainscore~excitability+age');
% lme
% % estimate effect size
% [beta, ~, stats] = fixedEffects(lme); 
% y = dataS.behav_brainscore;
% x1 = dataS.age;
% x2 = dataS.excitability;
% y_est= [double(x1) x2]*beta(2:3);
% y_x2 = [x2]*beta(3);
% residual = y - y_est;
% Eta2=var(y_est)/var(y) % total effect size
% Eta2_x2=var(y_x2)/(var(y_x2)+var(residual)) % partial effect size for effect of x2 on y

