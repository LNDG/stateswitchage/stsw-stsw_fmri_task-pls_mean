currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
rootpath = pwd;

pn.tools	 = fullfile(rootpath, 'tools');  addpath(genpath(pn.tools));
pn.data      = fullfile(rootpath, 'data');
pn.matpath   = fullfile(rootpath, 'data', 'pls');
pn.matpathOut   = fullfile(rootpath, 'data', 'pls');
pn.spmdata = fullfile(rootpath, '..', 'glm', 'data', 'firstlevel_betas');
pn.maskpath = fullfile(rootpath, 'data', 'standards', 'data', 'mni_icbm152_nlin_sym_09c');

filename = fullfile(rootpath, 'code', 'id_list_eegmr.txt');
fileID = fopen(filename);
IDs = textscan(fileID,'%s');
fclose(fileID);
IDs = IDs{1};

conditions = {'dim1'; 'dim2'; 'dim3'; 'dim4'};

[mask] = double(S_load_nii_2d(fullfile(pn.maskpath, 'mni_icbm152_gm_tal_nlin_sym_09c_MNI_3mm.nii')));

%% change the datamat information

for indID = 1:(numel(IDs))
    
   disp(['Processing subject ', IDs{indID}]);
   
    %% load _BfMRIsessiondata.mats variables to be changed 
   try
      a = load(fullfile(pn.matpath, ['task_', IDs{indID}, '_BfMRIsessiondata.mat']));
   catch ME
      disp (ME.message)
   end
   
   a.session_info.datamat_prefix = (['task_', IDs{indID}]);
   
   if strcmp(IDs{indID}, '2131') || strcmp(IDs{indID}, '2237')
        numOfRuns = 2;
    else
        numOfRuns = 4;
    end
   
   a.st_datamat = []; condInBeta = [1,5,9,13];
   for indCond = 1:4
       betaVols = [condInBeta(indCond), condInBeta(indCond)+45,condInBeta(indCond)+2*45, condInBeta(indCond)+3*45];
       for indBetaVols = 1:numOfRuns
           curVol = sprintf('%04d',betaVols(indBetaVols));
           [img] = double(S_load_nii_2d(fullfile(pn.spmdata, IDs{indID}, ['beta_',curVol,'.nii'])));
            img = img(mask==1,:); %restrict to final_coords
            beta(:,indBetaVols) = img;
       end
        a.st_datamat(indCond,:) = nanmean(beta,2); clear img;
        a.st_datamat(isnan(a.st_datamat)) = 0;
   end
   a.st_coords = find(mask==1)';

   % linear 1234 regression
   for indVox = 1:size(a.st_datamat,2)
       X = [1 1; 1 2; 1 3; 1 4];
       b = regress(a.st_datamat(1:4, indVox), X);
       a.st_datamat(5,indVox) = b(2);
   end
   
   %% edit conditions labels
   
   a.session_info.condition = cell(size(a.st_datamat,1),1);
   a.session_info.condition(1,1) = {'SDLoad1'};
   a.session_info.condition(2,1) = {'SDLoad2'};
   a.session_info.condition(3,1) = {'SDLoad3'};
   a.session_info.condition(4,1) = {'SDLoad4'};
   a.session_info.condition(5,1) = {'linear1234'};
   a.session_info.condition0 = a.session_info.condition;
     
   %% edit condition counters
   a.session_info.num_conditions  = numel(a.session_info.condition);
   a.session_info.num_conditions0 = numel(a.session_info.condition);
   
   %% add zero vector to st_datamat if condition(=session) not available
   % update condition variables according to count of conditions

   for indCond = size(a.session_info.condition_baseline,2)+1:numel(a.session_info.condition)

       a.session_info.condition_baseline{indCond}  = a.session_info.condition_baseline{1};
       a.session_info.condition_baseline0{indCond} = a.session_info.condition_baseline0{1};
   end
       
   % num_subj_cond AND st_evt_list: ones or numbers = count of conditions
   a.num_subj_cond = repmat(1,1,numel(a.session_info.condition));
   a.st_evt_list   = 1:1:numel(a.session_info.condition);

    %% save new data file
   save(fullfile(pn.matpathOut, ['task_', IDs{indID}, '_BfMRIsessiondata.mat']), '-struct', 'a');
    
   clear a
   
end