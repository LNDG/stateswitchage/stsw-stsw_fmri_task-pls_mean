currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
rootpath = pwd;

pn.data = fullfile(rootpath, 'data');
pn.tools = fullfile(rootpath, 'tools');
    addpath(fullfile(pn.tools, 'nifti_toolbox'));
    addpath(fullfile(pn.tools, 'preprocessing_tools'));
    
% load brainscores for younger adults (unthresholded)
%pn.YA = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B4_PLS_preproc2/B_data/SPM_STSWD_v3/behavPLS_STSWD_Mean_fullModel_N41_3mm_1000p000B_BfMRIbsr_lv1_unthresholded.nii';
pn.YA = fullfile(pn.data, 'pls', 'behavPLS_STSWD_SPM_YA_red_1group_3mm_1000p000B_BfMRIbsr_lv1.img');

% load brainscores for older adults (unthresholded)
%pn.OA = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B4_PLS_preproc2/B_data/SPM_STSWD_v3/behavPLS_STSWD_SPM_OA_v3_3mm_1000p000B_BfMRIbsr_lv1.img';
pn.OA = fullfile(pn.data, 'pls', 'behavPLS_STSWD_SPM_OA_red_1group_3mm_1000p000B_BfMRIbsr_lv1.img');

% load binary thalamic mask
pn.thalamus = fullfile(rootpath, '..', 'thalamic_nuclei', 'data', 'A_standards', 'Morel','AllNuclei_thr_MNI_3mm.nii');

% get GM mask coordinates used in PLS
maskpath = fullfile(rootpath, '..', 'thalamic_nuclei', 'data', 'A_standards', ...
    'mni_icbm152_nlin_sym_09c','mni_icbm152_gm_tal_nlin_sym_09c_MNI_3mm.nii');

[mask] = double(S_load_nii_2d(maskpath));

thalMask = S_load_nii_2d(pn.thalamus)';
brainscore_YA = S_load_nii_2d(pn.YA)';
brainscore_OA = S_load_nii_2d(pn.OA)';

brainscore_thal_YA = brainscore_YA(logical(thalMask & mask'));
brainscore_thal_OA = brainscore_OA(logical(thalMask & mask'));

brainscore_YA = brainscore_YA(logical(mask'));
brainscore_OA = brainscore_OA(logical(mask'));

% correlate BSRs wihtin thalamic mask between YAs and OAs

figure;
hold on;
a = brainscore_thal_YA;
b = brainscore_thal_OA;
ls_1 = polyval(polyfit(a, b,1),a); ls_1 = plot(a, ls_1, 'Color', 'k', 'LineWidth', 3);
scatter(a, b, 70, 'k','filled'); [r1, p] = corrcoef(a(~isnan(a)), b(~isnan(a)));
if p(2)>10^-3; tit = [' p = ', num2str(round(p(2),2))]; else; tit = sprintf(' p = %.1e',p(2)); end
leg = legend([ls_1], {['r = ', num2str(round(r1(2),2)), tit]},...
       'location', 'NorthWest'); legend('boxoff')        
xlabel('BSR Behav PLS YA'); ylabel('BSR Behav PLS OA')
title('Reproducibility of thalamic loadings')
set(findall(gcf,'-property','FontSize'),'FontSize',15)

figure;
hold on;
a = brainscore_YA;
a(a==0) = [];
b = brainscore_OA;
b(b==0) = [];
ls_1 = polyval(polyfit(a, b,1),a); ls_1 = plot(a, ls_1, 'Color', 'k', 'LineWidth', 3);
scatter(a, b, 5, 'k','filled'); [r1, p] = corrcoef(a(~isnan(a)), b(~isnan(a)));
if p(2)>10^-3; tit = [' p = ', num2str(round(p(2),2))]; else; tit = sprintf(' p = %.1e',p(2)); end
leg = legend([ls_1], {['r = ', num2str(round(r1(2),2)), tit]},...
       'location', 'NorthWest'); legend('boxoff')        
xlabel('BSR Behav PLS YA'); ylabel('BSR Behav PLS OA')
title('Reproducibility of brain-wide loadings')
set(findall(gcf,'-property','FontSize'),'FontSize',15)

