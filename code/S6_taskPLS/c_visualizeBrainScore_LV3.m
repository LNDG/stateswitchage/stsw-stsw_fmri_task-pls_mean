%% Plot brainscores outside PLS

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..', '..'))
rootpath = pwd;

pn.PLSfiles = fullfile(rootpath, 'data', 'pls');
pn.data = fullfile(rootpath, 'data'); 
pn.figures = fullfile(rootpath, 'figures'); 
pn.tools = fullfile(rootpath, 'tools'); 
    addpath(pn.tools);
    addpath(fullfile(pn.tools, 'barwitherr'));
    addpath(genpath(fullfile(pn.tools, 'RainCloudPlots')));
    addpath(fullfile(pn.tools, 'Cookdist'));

filename = fullfile(rootpath, 'code', 'id_list_eegmr.txt');
fileID = fopen(filename);
IDs_EEGMR = textscan(fileID,'%s');
fclose(fileID);
IDs_EEGMR = IDs_EEGMR{1};

IDs_YA = IDs_EEGMR(cellfun(@str2num, IDs_EEGMR)<2000);
IDs_OA = IDs_EEGMR(cellfun(@str2num, IDs_EEGMR)>2000);

load(fullfile(pn.PLSfiles, 'taskPLS_STSWD_SPM_YA_OA_3mm_1000P1000B_BfMRIresult.mat'))

groupsizes=result.num_subj_lst;
conditions=lv_evt_list;

conds = {'dim1'; 'dim2'; 'dim3'; 'dim4'};

LV = 3;

condData = []; uData = [];
for indGroup = 1:numel(groupsizes)
    if indGroup == 1
        relevantEntries = 1:groupsizes(1)*numel(conds);
    elseif indGroup == 2
        relevantEntries = groupsizes(1)*numel(conds)+1:...
             groupsizes(1)*numel(conds)+groupsizes(2)*numel(conds);
    end
    for indCond = 1:numel(conds)
        targetEntries = relevantEntries(conditions(relevantEntries)==indCond);        
        condData{indGroup}(indCond,:) = result.vsc(targetEntries,LV); % vsc: designscore
        uData{indGroup}(indCond,:) = result.usc(targetEntries,LV); % usc: brainscore
    end
end

% individualBrainScores = NaN(8,100);
% individualBehavScores = NaN(8,100);
% 
% individualBrainScores(1:4,1:size(uData{1},2)) = uData{1};
% individualBrainScores(5:end,1:size(uData{2},2)) = uData{2};
% 
% individualBehavScores(1:4,1:size(uData{1},2)) = condData{1};
% individualBehavScores(5:end,1:size(uData{2},2)) = condData{2};
% 
% indLV = 1;
% meanCent = result.boot_result.orig_usc(:,indLV)';
% ulusc_meanCent = result.boot_result.ulusc(:,indLV)';
% llusc_meanCent = result.boot_result.llusc(:,indLV)';
% 
% meanY = [nanmean(individualBrainScores,2)]';
% 
% ulusc_NonMeanCent = ulusc_meanCent+(meanY-meanCent);
% llusc_NonMeanCent = llusc_meanCent+(meanY-meanCent);
% 
% ulusc = ulusc_meanCent-meanCent;
% llusc = llusc_meanCent-meanCent;
% 
% % do not center data!
% diffCent = (meanY-meanCent);
% uData{1} = uData{1} + repmat(diffCent(1:4)',1,size(uData{1},2));
% uData{2} = uData{2} + repmat(diffCent(5:8)',1,size(uData{2},2));

% figure; 
% subplot(1,2,1); hold on; bar(meanCent); plot(ulusc_meanCent); plot(llusc_meanCent)
% subplot(1,2,2); hold on; bar(meanY); plot(ulusc_NonMeanCent); plot(llusc_NonMeanCent)

% errorY{1} = [llusc(1:4); ulusc(1:4)];
% errorY{2} = [llusc(5:end); ulusc(5:end)];

%% plot RainCloudPlot (within-subject centered)

    cBrew(1,:) = [0 0 0];
    cBrew(2,:) = [0 0 0];

    idx_outlier = cell(1); idx_standard = cell(1);
    for indGroup = 1:2
        dataToPlot = uData{indGroup}';
        % define outlier as lin. modulation that is more than three scaled median absolute deviations (MAD) away from the median
        X = [1 1; 1 2; 1 3; 1 4]; b=X\dataToPlot'; IndividualSlopes = b(2,:);
        outliers = isoutlier(IndividualSlopes, 'median');
        idx_outlier{indGroup} = find(outliers);
        idx_standard{indGroup} = find(outliers==0);
    end


    h = figure('units','centimeter','position',[0 0 25 10]);
    set(0, 'DefaultFigureRenderer', 'painters');
    for indGroup = 1:2
        dataToPlot = uData{indGroup}';
        % read into cell array of the appropriate dimensions
        data = []; data_ws = [];
        for i = 1:4
            for j = 1:1
                data{i, j} = dataToPlot(:,i);
                % individually demean for within-subject visualization
                data_ws{i, j} = dataToPlot(:,i)-...
                    nanmean(dataToPlot(:,:),2)+...
                    repmat(nanmean(nanmean(dataToPlot(:,:),2),1),size(dataToPlot(:,:),1),1);
                data_nooutlier{i, j} = data{i, j};
                data_nooutlier{i, j}(idx_outlier{indGroup}) = [];
                data_ws_nooutlier{i, j} = data_ws{i, j};
                data_ws_nooutlier{i, j}(idx_outlier{indGroup}) = [];
                % sort outliers to back in original data for improved plot overlap
                data_ws{i, j} = [data_ws{i, j}(idx_standard{indGroup}); data_ws{i, j}(idx_outlier{indGroup})];
            end
        end

        % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

        subplot(1,2,indGroup);
        set(gcf,'renderer','Painters')
            cla;
            cl = cBrew(indGroup,:);
            [~, dist] = rm_raincloud_fixedSpacing(data_ws, [.8 .8 .8],1);
            h_rc = rm_raincloud_fixedSpacing(data_ws_nooutlier, cl,1,[],[],[],dist);
            view([90 -90]);
            axis ij
        box(gca,'off')
        set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
        yticks = get(gca, 'ytick'); ylim([yticks(1)-(yticks(2)-yticks(1))./2, yticks(4)+(yticks(2)-yticks(1))./1.5]);

        minmax = [min(min(cat(2,data_ws{:}))), max(max(cat(2,data_ws{:})))];
        xlim(minmax+[-0.2*diff(minmax), 0.2*diff(minmax)])
        ylabel('Target load'); xlabel({'Brainscore'; '[Individually centered]'})

        % test linear effect
        curData = [data_nooutlier{1, 1}, data_nooutlier{2, 1}, data_nooutlier{3, 1}, data_nooutlier{4, 1}];
        X = [1 1; 1 2; 1 3; 1 4]; b=X\curData'; IndividualSlopes = b(2,:);
        [~, p, ci, stats] = ttest(IndividualSlopes);
        title(['M:', num2str(round(mean(IndividualSlopes),3)), '; p=', num2str(round(p,3))])
    end
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    figureName = ['s6a_pls_rcp_lv3'];
    saveas(h, fullfile(pn.figures, figureName), 'epsc');
    saveas(h, fullfile(pn.figures, figureName), 'png');

%% save individual brainscores & lin. modulation

LV3.data = cat(1,uData{1}',uData{2}');
X = [1 1; 1 2; 1 3; 1 4]; b=X\LV3.data'; 
LV3.linear = b(2,:);
LV3.IDs = IDs_EEGMR;

save(fullfile(pn.data, 's6a_LV3.mat'), 'LV3')
