currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..', '..'))
rootpath = pwd;

pn.PLSfiles = fullfile(rootpath, 'data', 'pls');
pn.plstoolbox = fullfile(rootpath, 'tools', 'pls'); addpath(genpath(pn.plstoolbox));

cd(pn.PLSfiles);

%batch_plsgui('taskPLS_STSWD_SPM_YA_OA_3mm_1000P1000B_BfMRIanalysis.txt')

plsgui
