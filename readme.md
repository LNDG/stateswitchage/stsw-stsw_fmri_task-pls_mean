# PLS overview

## 2nd level analysis: Multivariate modulation of BOLD responses

We investigated the multivariate modulation of the BOLD response at the 2nd level using PLS analyses (see Multivariate partial least squares analyses). Specifically, we probed the relationship between voxel-wise 1st level beta weights and probe uncertainty (i.e., task load level) within a task PLS. Next, we assessed the relationship between task-related BOLD signal changes and interindividual differences in the joint modulation of decision processes, cortical excitability, and pupil modulation by means of a behavioral PLS. For this, we first calculated linear slope coefficients for voxel-wise beta estimates. Then, we included behavioral variables including HDDM parameter estimates in the single-target condition, as well as linear changes with load, individual linear condition modulation of the following variables: multivariate spectral power, pupil dilation, 1/f modulation, and entropy residuals. Prior to these covariates in the model, we visually assessed whether the distribution of linear changes variables was approximately Gaussian. In the case of outliers (as observed for the SPMC, 1/f slopes, and entropy), we winsorized values to the 95th percentile. For visualization, spatial clusters were defined based on a minimum distance of 10 mm, and by exceeding a size of 25 voxels. We identified regions associated with peak activity based on cytoarchitectonic probabilistic maps implemented in the SPM Anatomy Toolbox (Version 2.2c) 113. If no assignment was found, the most proximal assignment within the cluster to the coordinates reported in Supplementary Table 1 was reported.

- Preproc v2
- Mean BOLD, SPM inputs

---

For these condition-wise data we use the following strategy:

- Condition info (when during which run) will be encoded in mat info files.
- These 'dummy files' will correspond to the input structures that the pls GUI expects.
- Relevant fMRI data will be replaced with the desired inputs (e.g., SD BOLD, 1st level SPM data, VarTbx data, etc ...).
- The most recent version of the PLS toolbox (which allows for .mat inputs) can be cloned from <https://git.mpib-berlin.mpg.de/LNDG/pls>.

**S0_collectInputData:**

- unzip preprocessed data to the input directory

**S1_createTaskInfoMats:**

- load a PLS mat template manually before running the function
- insert the relevant condition information from regressors to create the datamat in PLS (i.e. block onsets & durations)
- create individual input files to PLS (.mat): `<ID>_task_PLS_info.mat`

*Note: To get a balanced matrix of condition onsets etc. -1 is entered for conditions that were not present for the run. Note that PLS give a warning for those, but will automatically drop them from processing.*

**S2_make_meanbold_datamat_task:**

- use PLS to create the mean matrices: `task_<ID>_BfMRIsessiondata.mat`
- these mean matrices are currently not used later, but serve as 'dummies' that fulfill all the specification requirements of the PLS toolbox
- *this step will throw a warning for all the ‘-1’ entries (see above)*

**S3a_commonGMvoxels_summary:**

- create 3D summary niftys from 4D input, e.g., SD BOLD
- note that these are not necessarily used in the pls pipeline if you choose to model other inputs such as spm inputs (see S11 here)

**S3b_commonGMvoxels_summary:**

- extract coordinates where ALL subjects had non-zero values in MNI-based grey matter mask
- loads 3D summary SD niftys as input to not re-load large 4D files

**S4_create_sdbold_STSWD_task_v6:**

- create condition-wise SD BOLD mats as input for PLS
- global means per block (across voxels) were normalized to 10, with individual images expressed with respect to the difference to this global mean. Data were then concatenated across blocks and runs within condition. Finally, BOLD SD of each voxel was calculated across the concatenated time series.

## S11_SPM:

**S11A_createPLSinput_v3**

- this scripts replaces the information in `task_xxx_BfMRIsessiondata.mat` files with first level values from the SPM analysis.
- load 1st level beta images from PLS modelling
- create additional condition contrasts for PLS (e.g., behavioral contrasts)
- encoding of additional conditions in vanilla pls .mat files: 3D niftys are recoded to 1D vectors which are included in .mat

**S11B_createAnalysisScript_linear_win**

- fetch behavioral inputs, prepare them so that they can be (currently manually) copied into pls analysis .txt files

*At this point, manual pls model specification (`_BfMRIanalysis.txt`) is necessary*

**S11C_runPLS_analysis_v3**

- execute pls analysis by calling the corresponding model specification script
- open GUI, manually load resulting `_BfMRIresult.mat` pls output, inspect results

*The remaining scripts serve to visualize outputs within MATLAB.*

---

**v6**: Initial SD BOLD analysis using the v2 preprocessing data. Significant meancentered LV: stronger decrease in DMN dynamics with split attention. Behavioral PLS: stronger desynchronization is associated with better performers.

**v7**: For this analysis, volumes were exclusively extracted from volumes during stimulus presentation. The overall pattern of meancentered results did not change much, although the LV appeared more speckly. No significant LV was identified for predicting DDM and AMF though.