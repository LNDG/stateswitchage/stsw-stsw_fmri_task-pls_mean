These are reference images for working memory and state uncertainty.

Fig1_supp3a is from Muller et al., eLife

The uniformity test maps are from neurosynth.

behavPLS results are from the present PLS analysis.

The idea is the following: create masks of unique effects and intersections. Extract (absolute) brainscore data in these masks to quantify relative overlap with shared and unique activation patterns. Do the same for the Muller et al uncertainty map. 