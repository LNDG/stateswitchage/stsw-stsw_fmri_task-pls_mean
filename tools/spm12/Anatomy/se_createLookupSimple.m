clear,clc

MapNamen = spm_select(Inf,'mat',['Select Maps'],[],spm('Dir','se_anatomy'),'MPM.',1);


for mpmsselect = 1:size(MapNamen,1)

    MapName = strrep(MapNamen(mpmsselect,:),' ','');
    
    
    load(MapName);

    if strcmp(computer,'PCWIN')
        fid = fopen([spm_str_manip(MapName,'rt') '_lookup.txt'],'wt');
    else
        fid = fopen([spm_str_manip(MapName,'rt') '_lookup.txt'],'w+');
    end

    for i=1:size(MAP,2)
        name{i} = MAP(i).name;
        pmap{i} = spm_str_manip(MAP(i).ref,'rt');
        GV(i)   = MAP(i).GV;
        nr(i)   = i;
    end

    [names idx] = sort(name);
    GVs         = GV(idx);
    nrs         = nr(idx);
    pmap        = {pmap{idx}};

    fprintf(fid,[repmat('%s\t',1,3) '\n'],'Area','PMap','MPM-GV');


    for i=1:size(MAP,2)
        namen = repmat(' ',1,15);
        pma   = repmat(' ',1,15);
        namen(1:length(names{i})) = names{i};
        pma(1:length(pmap{i})) = pmap{i};
        index = nrs(i);

        fprintf(fid,[repmat('%s\t',1,3) '%s\n'],...
            namen,pma,int2str(MAP(index).GV));


        if round(i/3) == i/3
            fprintf(fid,'\n');
        end
    end
    status = fclose(fid);


end