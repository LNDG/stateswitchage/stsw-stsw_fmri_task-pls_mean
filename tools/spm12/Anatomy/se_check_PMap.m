function error = se_check_PMap(V)

if nargin<1, V= spm_vol(spm_select(Inf,'image','Select images')); end

em = {'The following PMaps are not compatible'};

error = 0;

for i=1:size(V)
    switch deblank(V(i).descrip)
        case 'SPM Anatomy toolbox PMap';
        otherwise
            error = 1;
            em{end+1} = V(i).fname;
    end
end
if error == 1
    spm('alert*',em)
end