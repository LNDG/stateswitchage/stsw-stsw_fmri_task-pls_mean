function Anatomy(varargin)

global defaults
global st

if strcmp(varargin,'select')

    
    try
        if numel(defaults)==0
            spm_defaults;
            %    spm('ChMod','FMRI')
        end
        defaults.oldDefaults = defaults;
    end

    
fg = se_figure('GetWin','Graphics');
delete(fg);
fg = se_figure('GetWin','Graphics');
if isempty(fg), error('Can''t create graphics window'); end
se_figure('Clear','Graphics');
set(gcf,'DefaultUicontrolFontSize',spm('FontSizes',get(gcf,'DefaultUicontrolFontSize')));

WS = spm('WinScale');

    st.SPM = spm('FnBanner');

    uicontrol(fg,'Style','Text','Position',[25 780 550 55].*WS,'String','SPM ANATOMY TOOLBOX  v1.8',...
             'FontSize',25,'FontWeight','bold','BackgroundColor',[1 1 1],'HorizontalAlignment','Center');

   
   
   
uicontrol(fg,'Style','PushButton','Units','normalized','Position',[.35 .82 .3 .03],'String','Important Notes','FontSize',20,...
	'Callback','se_note','FontWeight','bold','FontSize',spm('FontSizes',14),'ForegroundColor','g');





uicontrol(fg,'Style','PushButton','Units','normalized','Position',[.05 .70 .9 .055],'Callback','se_explore_pmap;',...
    'String','Visualisation and statistics of cytoarchitectonic probabilistic maps','FontSize',spm('FontSizes',14));

%uicontrol(fg,'Style','PushButton','Units','normalized','Position',[.05 .70-1*0.075 .9 0.055],'Callback','se_create_MPM;',...
%    'String','Calculation of maximum probability maps','FontSize',spm('FontSizes',14));

uicontrol(fg,'Style','PushButton','Units','normalized','Position',[.05 .70-1*0.075 .9 0.055],'Callback','se_anatomy(''initP'');',...
    'String',{'Cytoarchitectonic probabilities at defined MNI coordinates'},'FontSize',spm('FontSizes',14));

uicontrol(fg,'Style','PushButton','Units','normalized','Position',[.05 .70-2*0.075 .9 0.055],'Callback','se_TabList;',...
    'String',{'Cytoarchitectonic probabilities at defined MNI coordinates (batch)'},'FontSize',spm('FontSizes',14));

uicontrol(fg,'Style','PushButton','Units','normalized','Position',[.05 .70-3*0.075 .9 0.055],'Callback','se_anatomy(''initO'');',...
    'String',{'Overlap between structure and function (SPM/images)'},'FontSize',spm('FontSizes',14));

uicontrol(fg,'Style','PushButton','Units','normalized','Position',[.05 .70-4*0.075 .9 0.055],'Callback','se_anatomy(''initG'');',...
    'String',{'Mean response (group analysis)'},'FontSize',spm('FontSizes',14));
         
uicontrol(fg,'Style','PushButton','Units','normalized','Position',[.05 .70-5*0.075 .9 0.055],'Callback','se_anatomy(''initA'');',...
    'String',{'Display functional response of anatomical areas'},'FontSize',spm('FontSizes',14));

uicontrol(fg,'Style','PushButton','Units','normalized','Position',[.05 .70-6*0.075 .9 0.055],'Callback','se_Achart;',...
    'String',{'Functional response of anatomical areas (summary)'},'FontSize',spm('FontSizes',14));

uicontrol(fg,'Style','PushButton','Units','normalized','Position',[.05 .70-7*0.075 .9 0.055],'Callback','se_createROI;',...
    'String',{'Create anatomical ROIs'},'FontSize',spm('FontSizes',14));

uicontrol(fg,'Style','PushButton','Units','normalized','Position',[.05 .70-8*0.075 .9 0.055],'Callback','se_ROIextract;',...
    'String',{'Calculate image means within anatomical ROIs'},'FontSize',spm('FontSizes',14));


uicontrol(fg,'Style','PushButton','Units','normalized','Position',[.9 .01 .05 .03],'Callback','se_exit;',...
    'String',{'Quit'},'FontSize',spm('FontSizes',10));


axis off

else
    se_note
end