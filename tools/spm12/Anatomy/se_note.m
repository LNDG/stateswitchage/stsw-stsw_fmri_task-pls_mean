fg = se_figure('GetWin','Graphics');
se_figure('Clear','Graphics');
WS = spm('WinScale');
uicontrol(fg,'Style','Text','Position',[25 780 550 55].*WS,'String','SPM ANATOMY TOOLBOX  v1.8',...
             'FontSize',25,'FontWeight','bold','BackgroundColor',[1 1 1],'HorizontalAlignment','Center');

         
uicontrol(fg,'Style','Text','Position',[25 675 550 100].*WS,'String','Primary references:','FontWeight','bold','FontSize',11,'BackgroundColor',[1 1 1]);
uicontrol(fg,'Style','Text','Position',[25 600+5 550 150].*WS,'String',...
{'Eickhoff SB et al.:  "A new SPM toolbox..."  (2005) NeuroImage 25(4): 1325-1335';...
'Eickhoff SB et al.:  "Testing anatomically specified hypotheses..."  (2006) NeuroImage 32(2): 570-82';...
'Eickhoff SB et al.:  "Assignment of functional activations..."  (2007) NeuroImage 36(3): 511-521 '},...
  'FontSize',9,'BackgroundColor',[1 1 1],'HorizontalAlignment','center');

   uicontrol(fg,'Style','Text','Position',[25 650 550 50].*WS,'String',...
    'Contact: Simon Eickhoff  (s.eickhoff@fz-juelich.de)','FontSize',10,'BackgroundColor',[1 1 1],...
     'HorizontalAlignment','center','FontWeight','bold');


 
 uicontrol(fg,'Style','Text','Position',[25 595 550 55].*WS,'String','Publications describing included probabilistic maps:',...
             'FontSize',12,'BackgroundColor',[1 1 1],'FontWeight','bold');

uicontrol(fg,'Style','Text','Position',[20+30 60 200 560].*WS,'String',...
    {'Auditory cortex';
     '';
     'Broca''s area';
     '';
     'Motor cortex';
     '';
     '';
     'Somatosensory cortex';
     ''
     ''
     'Parietal operculum / SII'
     '';
     'Parietal cortex'
     '';
     '';
     'Intraparietal sulcus'
     '';
     '';
     'Insula'
     '';
     'Amygdala'
     'Hippocampus';
     '';
     'Visual cortex';
     ''; '';'';
     'Fiber tracts';...
          '';
     'Cerebellum';...
          '';
     'Thalamus';...
          },'FontSize',8,'BackgroundColor',[1 1 1],...
     'HorizontalAlignment','left');
 
uicontrol(fg,'Style','Text','Position',[180 60 230 560].*WS,'String',...
    {'TE 1.0, TE 1.1, TE 1.2';
     '';
     'BA 44, BA 45';
     '';
     'BA 4a, BA 4p';
     'BA 6';
     '';
     'BA 3a, BA 3b, BA 1';
     'BA 2';
     ''
     'OP 1, OP 2, OP 3, OP 4'
     '';     
     'PFt, PF, PFm, PFcm, PFop, PGa, PGp'
     '5Ci, 5L, 5M, 7A, 7M, 7P, 7PC'
     '';
     'hIP1, hIP2'
     'hIP3'
     '';
     'Ig1, Ig2, Id1'
     '';
     'CM/LB/SF'
     'FD/CA/SUB/EC/HATA';
     '';
     'BA 17, BA 18';
     'hOC5'; 'hOC3v / hOC4v'; '';
     '13 structures';
     ''; '18 structures';
     ''; '7 connectivity zones'},'FontSize',8,'BackgroundColor',[1 1 1],...
     'HorizontalAlignment','left');
 
uicontrol(fg,'Style','Text','Position',[350+30 60 300 560].*WS,'String',...
    {'Morosan et al., NeuroImage 2001';
     '';
     'Amunts et al., J Comp Neurol 1999';
     '';
     'Geyer et al., Nature 1996';
     'S. Geyer, Springer press 2003';
     '';
     'Geyer et al., NeuroImage, 1999, 2000';
     'Grefkes et al., NeuroImage 2001';
     '';
     'Eickhoff et al., Cerebral Cortex 2006a,b'
     '';
     'Caspers et al., Neuroimage 2007, BSF 2008'
     'Scheperjans et al., Cerebral Cortex 2008a,b'
     '';
     'Choi et al., J Comp Neurol 2006'
     'Scheperjans et al., Cerebral Cortex 2008a,b'
     '';
     'Kurth et al., Cerebral Cortex 2010'
     '';
     'Amunts et al., Anat Embryol 2005'
     'Amunts et al., Anat Embryol 2005'
     '';
     'Amunts et al., NeuroImage 2000';
     'Malikovic et al., Cerebral Cortex 2006';
     'Rottschy et al., Hum Brain Mapp 2007';
     '';'B�rgel et al., NeuroImage 1999, 2006';
     '';
     'Diedrichsen et al., NeuroImage 2009'
     '';
     'Behrens et al., Nat Neurosci 2003'},'FontSize',8,'BackgroundColor',[1 1 1],...
     'HorizontalAlignment','left');

 uicontrol(fg,'Style','Text','Position',[40 40 500 20].*WS,'String','Other areas may only be used with authors'' permission !',...
             'FontSize',12,'BackgroundColor',[1 1 1],'FontWeight','bold');

uicontrol(fg,'Style','PushButton', 'Position',[210 10 160 28].*WS,'String','Start','FontSize',20,...
	'Callback','Anatomy(''select'')','FontWeight','bold','FontSize',14,'ForegroundColor',[0 .5 0]);
