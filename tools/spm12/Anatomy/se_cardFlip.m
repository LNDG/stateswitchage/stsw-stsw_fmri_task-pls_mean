function se_cardFlip

global st;
global MAP;
global SPM;
global xSPM;
global CLUSTER;
global displayType;
global group
global defaults;

if numel(CLUSTER)>0
    try
        thres = strrep(SPM.thresDef,'0.','');, thres = strrep(thres,'(','-'); thres = strrep(thres,')',''); thres = strrep(thres,'p<','_p'); thres = strrep(thres,'P>','_P');
    catch
        thres = '';
    end

    filename = [strrep(SPM.title,' ','_') strrep(thres,' ','') '.ps'];
    filename = strrep(filename,'>','vs');    filename = strrep(filename,'<','vs');
    filename = strrep(filename,')','-');     filename = strrep(filename,'(','-');
    filename = strrep(filename,'/','-');     filename = strrep(filename,'\','-');


    for cx = 1:numel(CLUSTER)
        set(st.cluster,'Value',cx+1)
        se_anatomy('setposclust')

        figure(st.Fgraph)
        print 'Anatomy.ps' -noui -painters -dpsc2 -append

        for cy=1:numel(CLUSTER(cx).maxZ)
            set(st.maxima,'Value',cy+1)
            se_anatomy('setposmax')
            se_figure('printflip')
        end
    end

    movefile('Anatomy.ps',filename)

end