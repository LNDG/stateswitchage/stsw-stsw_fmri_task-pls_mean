function se_createROI(varargin)

try
    warning off MATLAB:divideByZero
end

global group
global st
global MAP
global ArResp
global index
global displayType;
global defaults

st.SPM = spm('ver');

if (nargin==0), Action = 'init'; else, Action = varargin{1}; end

switch lower(Action), 
    case 'clear'
        if isfield(st,'figs'); delete(st.figs); st = rmfield(st,'figs');; end
        set(st.area(:),'Value',0)
    case 'calculate'
        a = get(st.area,'Value'); for fn = 1:size(a,1); b(fn) = a{fn}; end
        if find(b)
            se_figure('getWin','Interactive');
            ROIname = spm_input('ROI file name',1,'s','');
            fwhm = 0; %spm_input('smoothing {FWHM in mm, 0 for none}','+1','i',0);
            
            Vo = MAP(1).MaxMap;  
            switch upper(st.SPM);
                case 'SPM5'
                    Vo.dim = Vo.dim(1:3);
                    Vo.dt  = [spm_type('uint8') spm_platform('bigend')];
                case 'SPM8'
                    Vo.dim = Vo.dim(1:3);
                    Vo.dt  = [spm_type('uint8') spm_platform('bigend')];
                otherwise
                    Vo.dim(4) = spm_type('uint8'); 
            end
                
            cMode = spm_input_ui('Output space','!+1','b','Anatomical MNI | MNI',[1 2],2,'Select mode');
           
            if cMode == 2
                Vo.mat(2,4) = Vo.mat(2,4)+4;
                Vo.mat(3,4) = Vo.mat(3,4)-5;
                ROIname = [ROIname '_MNI'];
            else
                ROIname = [ROIname '_aMNI'];
            end
            
            Vo.fname = ['ROI_' ROIname '.img']; Vo.M = Vo.mat; Vo.descrip = 'Anatomy toolbox ROI';
            
            Vtmp= zeros(MAP(1).MaxMap.dim(1:3));
            todo = find(b);
            for i=1:size(todo,2)
                if rem(todo(i),2); 
                    vox = MAP((todo(i)+1)/2).XYZ(:,MAP((todo(i)+1)/2).LR == -1);
                else; 
                    vox = MAP(todo(i)/2).XYZ(:,MAP(todo(i)/2).LR == 1);
                end;
                
                A     = spm_clusters(vox); Q     = [];
                for i = 1:max(A); j = find(A == i); if length(j) >= 25; Q = [Q j]; end; end
                vox   = vox(:,Q);

                for vx=1:size(vox,2); 
                    Vtmp(vox(1,vx),vox(2,vx),vox(3,vx)) = 1; 
                end
            end
            if strcmp(spm('FnBanner'),'SPM2');
                try
                    flip = defaults.analyze.flip;
                catch
                    try
                        spm_defaults;
                        flip = defaults.analyze.flip;
                    catch
                        flip = 0;
                    end
                end
            end
            defaults.analyze.flip = 0;
            Vo = spm_create_vol(Vo);
            for p=1:Vo.dim(3),
                Vo = spm_write_plane(Vo,Vtmp(:,:,p),p);
            end;

            if strcmpi(st.SPM,'spm5') | strcmpi(st.SPM,'spm8')
            else, try, Vo= spm_close_vol(Vo); end, end



            try, defaults.analyze.flip = flip; end
            mat = Vo.mat; M = Vo.M;
            save(['ROI_' ROIname '.mat'],'mat','M');
            se_createROI('clear')
            se_figure('Clear','Interactive');
        else
            spm('alert!','No area selected',sqrt(-1));
        end

        
    case 'calculati'
        a = get(st.area,'Value'); for fn = 1:size(a,1); b(fn) = a{fn}; end
        if find(b)
            se_figure('getWin','Interactive');
            fwhm = 0; %spm_input('smoothing {FWHM in mm, 0 for none}','+1','i',0);
            
            Vo = MAP(1).MaxMap;  
            switch upper(spm('ver'));
                case 'SPM5'
                    Vo.dim = Vo.dim(1:3);
                    Vo.dt  = [spm_type('uint8') spm_platform('bigend')];
                case 'SPM8'
                    Vo.dim = Vo.dim(1:3);
                    Vo.dt  = [spm_type('uint8') spm_platform('bigend')];
                otherwise
                    Vo.dim(4) = spm_type('uint8'); 
            end
                
            cMode = spm_input_ui('Output space','!+1','b','Anatomical | MNI',[1 2],2,'Select mode');
            
            if cMode == 2
                Vo.mat(2,4) = Vo.mat(2,4)+4;
                Vo.mat(3,4) = Vo.mat(3,4)-5;
            end

            Vtmp= zeros(MAP(1).MaxMap.dim(1:3));
            todo = find(b);
            for i=1:size(todo,2)
                Vtmp= zeros(MAP(1).MaxMap.dim(1:3));
                if rem(todo(i),2);
                    vox = MAP((todo(i)+1)/2).XYZ(:,MAP((todo(i)+1)/2).LR == -1);
                    ROIname = [spm_str_manip(MAP((todo(i)+1)/2).ref,'rt') '_L'];
                else; 
                    vox = MAP(todo(i)/2).XYZ(:,MAP(todo(i)/2).LR == 1);
                    ROIname = [spm_str_manip(MAP((todo(i))/2).ref,'rt') '_R'];
                end;
                
                
                
                if cMode == 2
                    ROIname = [ROIname '_MNI'];
                else
                    ROIname = [ROIname '_aMNI'];
                end
                Vo.fname = ['ROI_' ROIname '.img']; Vo.M = Vo.mat; Vo.descrip = 'Anatomy toolbox ROI';


                A     = spm_clusters(vox); Q     = [];
                for i = 1:max(A); j = find(A == i); if length(j) >= 25; Q = [Q j]; end; end
                vox   = vox(:,Q);

                for vx=1:size(vox,2);
                    Vtmp(vox(1,vx),vox(2,vx),vox(3,vx)) = 1;
                end

                if strcmp(spm('FnBanner'),'SPM2');
                    try
                        flip = defaults.analyze.flip;
                    catch
                        try
                            spm_defaults;
                            flip = defaults.analyze.flip;
                        catch
                            flip = 0;
                        end
                    end
                end
                defaults.analyze.flip = 0;
                Vo = spm_create_vol(Vo);
                for p=1:Vo.dim(3),
                    Vo = spm_write_plane(Vo,Vtmp(:,:,p),p);
                end;
            end



            if strcmpi(st.SPM,'spm5') | strcmpi(st.SPM,'spm8')
            else, try, Vo= spm_close_vol(Vo); end, end



            try, defaults.analyze.flip = flip; end
            mat = Vo.mat; M = Vo.M;
            save(['ROI_' ROIname '.mat'],'mat','M');
            se_createROI('clear')
            se_figure('Clear','Interactive');
        else
            spm('alert!','No area selected',sqrt(-1));
        end
        
        
    case 'init'
        fg = se_figure('GetWin','Graphics');
        Finter = spm('CreateIntWin','on');	
        se_figure('Clear','Graphics');
        load(spm_select(1,'mat',['Select Map'],[],spm('Dir','se_anatomy'),'MPM.',1));
		FS        = spm('FontSizes');
		hFS = FS(get(gcf,'DefaultUicontrolFontSize'));
        [B,index] = sortrows(char(MAP.name));
        
        uicontrol(fg,'Style','Frame','Units','normalized','Position',[0 .85-.02*ceil(size(MAP,2)/2) 0.65 .115+.02*(ceil(size(MAP,2)/2))]);
        uicontrol(fg,'Style','Pushbutton','Units','normalized','Position',[0.01 .93 .28 .03],'ForegroundColor','r',...
            'FontWeight','bold','String','Create composite','Callback','se_createROI(''calculate'')','ToolTipString','Creates an ROI containing all selected areas');
        uicontrol(fg,'Style','Pushbutton','Units','normalized','Position',[0.31 .93 .28 .03],'ForegroundColor','r',...
            'FontWeight','bold','String','Create individual','Callback','se_createROI(''calculati'')','ToolTipString','Creates ROI or each if the selected areas');

        uicontrol(fg,'Style','text','Units','normalized','Position',[0.01 .895 .029 .02],'FontWeight','bold','String','L','HorizontalAlignment','center');
        uicontrol(fg,'Style','text','Units','normalized','Position',[0.04 .895 .029 .02],'FontWeight','bold','String','R','HorizontalAlignment','center');
        
        uicontrol(fg,'Style','text','Units','normalized','Position',[0.305 .895 .029 .02],'FontWeight','bold','String','L','HorizontalAlignment','center');
        uicontrol(fg,'Style','text','Units','normalized','Position',[0.335 .895 .029 .02],'FontWeight','bold','String','R','HorizontalAlignment','center');
        
        uicontrol(fg,'Style','text','Units','normalized','Position',[0.01 .87-.02*ceil(size(MAP,2)/2) .029 .02],'FontWeight','bold','String','L','HorizontalAlignment','center');
        uicontrol(fg,'Style','text','Units','normalized','Position',[0.04 .87-.02*ceil(size(MAP,2)/2) .029 .02],'FontWeight','bold','String','R','HorizontalAlignment','center');

        uicontrol(fg,'Style','text','Units','normalized','Position',[0.305 .87-.02*ceil(size(MAP,2)/2) .029 .02],'FontWeight','bold','String','L','HorizontalAlignment','center');
        uicontrol(fg,'Style','text','Units','normalized','Position',[0.335 .87-.02*ceil(size(MAP,2)/2) .029 .02],'FontWeight','bold','String','R','HorizontalAlignment','center');

        uicontrol(fg,'Style','Pushbutton','Units','normalized','Position',[.1 .86-.02*ceil(size(MAP,2)/2) .11 .02],...
            'String','CLEAR','Callback','se_createROI(''clear'')');
        uicontrol(fg,'Style','PushButton','Units','normalized','Position',[.4 .86-.02*ceil(size(MAP,2)/2) .11 .02],'ForegroundColor','r','FontWeight','bold',...
          	'String','EXIT','Callback','se_createROI(''exit'');','ToolTipString','quit');

        
        for i = 1:size(MAP,2)
            if i<=ceil(size(MAP,2)/2)
                st.area((index(i)*2)-1) = uicontrol(fg,'Style','checkbox','Units','normalized','Position',[0.01 .893-.02*i .025 .025],'Callback','se_createROI(''checked'')'); 
                st.area((index(i)*2)) =   uicontrol(fg,'Style','checkbox','Units','normalized','Position',[0.04 .893-.02*i .025 .025],'Callback','se_createROI(''checked'')');
                uicontrol(fg,'Style','text','Units','normalized','Position',[0.075 .89-.02*i .34 .025],'String',{MAP(index(i)).name},'HorizontalAlignment','left');
            else
                st.area((index(i)*2)-1) = uicontrol(fg,'Style','checkbox','Units','normalized','Position',[0.31 .893-.02*(i-ceil(size(MAP,2)/2)) .025 .025],'Callback','se_createROI(''checked'')'); 
                st.area((index(i)*2)) =   uicontrol(fg,'Style','checkbox','Units','normalized','Position',[0.34 .893-.02*(i-ceil(size(MAP,2)/2)) .025 .025],'Callback','se_createROI(''checked'')');
                uicontrol(fg,'Style','text','Units','normalized','Position',[0.375 .89-.02*(i-ceil(size(MAP,2)/2)) .24 .025],'String',{MAP(index(i)).name},'HorizontalAlignment','left');
            end
            ArResp(((index(i)*2)-1)).nvox = 0; ArResp(((index(i)*2))).nvox = 0;
        end
        
        
        
       
    case 'exit'
        se_figure('Clear','Graphics');
        clear all;
        Anatomy('select');
end


