function varargout = se_orthviews(action,varargin)
% Display Orthogonal Views of a Normalized Image
% FORMAT H = se_orthviews('Image',filename[,position])
% filename - name of image to display
% area     - position of image
%            -  area(1) - position x
%            -  area(2) - position y
%            -  area(3) - size x
%            -  area(4) - size y
% H        - handle for ortho sections
% FORMAT se_orthviews('BB',bb)
% bb       - bounding box
%            [loX loY loZ
%             hiX hiY hiZ]
%
% FORMAT se_orthviews('Redraw')
% Redraws the images
%
% FORMAT se_orthviews('Reposition',centre)
% centre   - X, Y & Z coordinates of centre voxel
%
% FORMAT se_orthviews('Space'[,handle])
% handle   - the view to define the space by
% with no arguments - puts things into mm space
%
% FORMAT se_orthviews('MaxBB')
% sets the bounding box big enough display the whole of all images
%
% FORMAT se_orthviews('Resolution',res)
% res      - resolution (mm)
%
% FORMAT se_orthviews('Delete', handle)
% handle   - image number to delete
%
% FORMAT se_orthviews('Reset')
% clears the orthogonal views
%
% FORMAT se_orthviews('Pos')
% returns the co-ordinate of the crosshairs in millimetres in the
% standard space.
%
% FORMAT se_orthviews('Pos', i)
% returns the voxel co-ordinate of the crosshairs in the image in the
% ith orthogonal section.
%
% FORMAT se_orthviews('Xhairs','off') OR se_orthviews('Xhairs')
% disables the cross-hairs on the display.
%
% FORMAT se_orthviews('Xhairs','on')
% enables the cross-hairs.
%
% FORMAT se_orthviews('Interp',hld)
% sets the hold value to hld (see spm_slice_vol).
%
% FORMAT se_orthviews('AddBlobs',handle,XYZ,Z,mat)
% Adds blobs from a pointlist to the image specified by the handle(s).
% handle   - image number to add blobs to
% XYZ      - blob voxel locations
% Z        - blob voxel intensities
% mat      - matrix from voxels to millimeters of blob.
% name     - a name for this blob
% This method only adds one set of blobs, and displays them using a
% split colour table.
%
% FORMAT se_orthviews('AddColouredBlobs',handle,XYZ,Z,mat,colour,name)
% Adds blobs from a pointlist to the image specified by the handle(s).
% handle   - image number to add blobs to
% XYZ      - blob voxel locations
% Z        - blob voxel intensities
% mat      - matrix from voxels to millimeters of blob.
% colour   - the 3 vector containing the colour that the blobs should be
% name     - a name for this blob
% Several sets of blobs can be added in this way, and it uses full colour.
% Although it may not be particularly attractive on the screen, the colour
% blobs print well.
%
% FORMAT se_orthviews('RemoveBlobs',handle)
% Removes all blobs from the image specified by the handle(s).
%
% FORMAT se_orthviews('Register',hReg)
% hReg      - Handle of HandleGraphics object to build registry in.
% See spm_XYZreg for more information.
%
%_______________________________________________________________________
% @(#)se_orthviews.m	2.25 John Ashburner & Matthew Brett 01/03/19


% The basic fields of st are:
%         n        - the number of images currently being displayed
%         vols     - a cell array containing the data on each of the
%                    displayed images.
%         Space    - a mapping between the displayed images and the
%                    mm space of each image.
%         bb       - the bounding box of the displayed images.
%         centre   - the current centre of the orthogonal views
%         callback - a callback to be evaluated on a button-click.
%         xhairs   - crosshairs off/on
%         hld      - the interpolation method
%         fig      - the figure that everything is displayed in
%         mode     - the position/orientation of the sagittal view.
%                    - currently always 1
%
%         st.registry.hReg \_ See spm_XYZreg for documentation
%         st.registry.hMe  /
%
% For each of the displayed images, there is a non-empty entry in the
% vols cell array.  Handles returned by "se_orthviews('Image',.....)"
% indicate the position in the cell array of the newly created ortho-view.
% Operations on each ortho-view require the handle to be passed.
%
% When a new image is displayed, the cell entry contains the information
% returned by spm_vol (type help spm_vol for more info).  In addition,
% there are a few other fields, some of which I will document here:
%
%         premul - a matrix to premultiply the .mat field by.  Useful
%                  for re-orienting images.
%         window - either 'auto' or an intensity range to display the
%                  image with.
%         mapping- Mapping of image intensities to grey values. Currently
%                  one of 'linear', 'histeq', loghisteq',
%                  'quadhisteq'. Default is 'linear'.
%                  Histogram equalisation depends on the image toolbox
%                  and is only available if there is a license available
%                  for it.
%         ax     - a cell array containing an element for the three
%                  views.  The fields of each element are handles for
%                  the axis, image and crosshairs.
%
%         blobs - optional.  Is there for using to superimpose blobs.
%                 vol     - 3D array of image data
%                 mat     - a mapping from vox-to-mm (see spm_vol, or
%                           help on image formats).
%                 max     - maximum intensity for scaling to.  If it
%                           does not exist, then images are auto-scaled.
%
%                 There are two colouring modes: full colour, and split
%                 colour.  When using full colour, there should be a
%                 'colour' field for each cell element.  When using
%                 split colourscale, there is a handle for the colorbar
%                 axis.
%
%                 colour  - if it exists it contains the
%                           red,green,blue that the blobs should be
%                           displayed in.
%                 cbar    - handle for colorbar (for split colourscale).

global st;

if isempty(st), reset_st; end;

spm('Pointer','watch');

if nargin == 0, action = ''; end;
action = lower(action);

switch lower(action),
    case 'image',
        H = specify_image(varargin{1});
        if ~isempty(H)
            st.vols{H}.area = [0 0 1 1];
            if numel(varargin)>=2, st.vols{H}.area = varargin{2}; end;
            if isempty(st.bb), st.bb = maxbb; end;
            bbox;
        end;
        varargout{1} = H;
        st.centre    = mean(maxbb);
        redraw_all
        
    case 'bb',
        if ~isempty(varargin) && all(size(varargin{1})==[2 3]), st.bb = varargin{1}; end;
        bbox;
        redraw_all;
        
    case 'redraw',
        redraw_all;
        eval(st.callback);
        if isfield(st,'registry'),
            spm_XYZreg('SetCoords',st.centre,st.registry.hReg,st.registry.hMe);
        end;
        
    case 'reposition',
        if numel(varargin)<1, tmp = findcent;
        else tmp = varargin{1}; end;
        if isequal(size(tmp),[3 1]),
            st.centre = tmp(1:3);
        end;
        redraw_all;
        eval(st.callback);
        if isfield(st,'registry'),
            spm_XYZreg('SetCoords',st.centre,st.registry.hReg,st.registry.hMe);
        end;
        
    case 'setcoords',
        st.centre = varargin{1};
        st.centre = st.centre(:);
        redraw_all;
        eval(st.callback);
        
    case 'space',
        if numel(varargin)<1,
            st.Space = eye(4);
            st.bb = maxbb;
            bbox;
            redraw_all;
        else
            space(varargin{:});
            bbox;
            redraw_all;
        end;
        
    case 'maxbb',
        st.bb = maxbb;
        bbox;
        redraw_all;
        
    case 'resolution',
        resolution(varargin{1});
        bbox;
        redraw_all;
        
    case 'window',
        if numel(varargin)<2,
            win = 'auto';
        elseif numel(varargin{2})==2,
            win = varargin{2};
        end;
        for i=valid_handles(varargin{1}),
            st.vols{i}.window = win;
        end;
        redraw(varargin{1});
        
    case 'delete',
        my_delete(varargin{1});
        
    case 'move',
        move(varargin{1},varargin{2});
        % redraw_all;
        
    case 'reset',
        my_reset;
        
    case 'pos',
        if isempty(varargin),
            H = st.centre(:);
        else
            H = pos(varargin{1});
        end;
        varargout{1} = H;
        
    case 'interp',
        st.hld = varargin{1};
        redraw_all;
        
    case 'xhairs',
        xhairs(varargin{1});
        
    case 'register',
        register(varargin{1});
        
    case 'addblobs',
        addblobs(varargin{:});
        redraw(varargin{1});
        
    case 'addcolouredblobs',
        addcolouredblobs(varargin{:});
        % redraw(varargin{1});
        
    case 'addimage',
        addimage(varargin{1}, varargin{2});
        redraw(varargin{1});
        
    case 'addcolouredimage',
        addcolouredimage(varargin{1}, varargin{2},varargin{3});
        % redraw(varargin{1});
        
    case 'addtruecolourimage',
        % se_orthviews('Addtruecolourimage',handle,filename,colourmap,prop,mx,mn)
        % Adds blobs from an image in true colour
        % handle   - image number to add blobs to [default 1]
        % filename of image containing blob data [default - request via GUI]
        % colourmap - colormap to display blobs in [GUI input]
        % prop - intensity proportion of activation cf grayscale [0.4]
        % mx   - maximum intensity to scale to [maximum value in activation image]
        % mn   - minimum intensity to scale to [minimum value in activation image]
        %
        if nargin < 2
            varargin(1) = {1};
        end
        if nargin < 3
            varargin(2) = {spm_select(1, 'image', 'Image with activation signal')};
        end
        if nargin < 4
            actc = [];
            while isempty(actc)
                actc = getcmap(spm_input('Colourmap for activation image', '+1','s'));
            end
            varargin(3) = {actc};
        end
        if nargin < 5
            varargin(4) = {0.4};
        end
        if nargin < 6
            actv = spm_vol(varargin{2});
            varargin(5) = {max([eps maxval(actv)])};
        end
        if nargin < 7
            varargin(6) = {min([0 minval(actv)])};
        end
        
        addtruecolourimage(varargin{1}, varargin{2},varargin{3}, varargin{4}, ...
            varargin{5}, varargin{6});
        redraw(varargin{1});
        
    case 'rmblobs',
        rmblobs(varargin{1});
        redraw(varargin{1});
        
    otherwise,
        warning('Unknown action string')
end;

spm('Pointer');
return;


%_______________________________________________________________________
%_______________________________________________________________________
function addblobs(handle, xyz, t, mat, name)
global st
global displayType;
if nargin < 5
    name = '';
end;
for i=valid_handles(handle),
    if ~isempty(xyz),
        rcp      = round(xyz);
        dim      = max(rcp,[],2)';
        off      = rcp(1,:) + dim(1)*(rcp(2,:)-1 + dim(2)*(rcp(3,:)-1));
        vol      = zeros(dim)+NaN;
        vol(off) = t;
        vol      = reshape(vol,dim);
        st.vols{i}.blobs=cell(1,1);
        mx = max([eps max(t)]);
        mn = min(t(t>0));
        if st.mode == 0,
            axpos = get(st.vols{i}.ax{2}.ax,'Position');
        else
            axpos = get(st.vols{i}.ax{1}.ax,'Position');
        end;
        
        if strcmp(displayType,'PM')
            ax = axes('Parent',st.fig,'Position',[(axpos(1)+axpos(3)+0.1) (axpos(2)+0.005) 0.05 (axpos(4)-0.01)],...
                      'Box','on');
            
            imag = [];
            for xi=1:10
                imag = [imag; repmat(xi*10,10,10)];
            end
            image(imag + 64,'Parent',ax);
            set(ax,'YDir','normal','XTickLabel',[],'XTick',[]);
            st.vols{i}.blobs{1} = struct('vol',vol,'mat',mat,'cbar',ax,'max',mx, 'min',mn,'name',name);
        else
            st.vols{i}.blobs{1} = struct('vol',vol,'mat',mat,'max',mx, 'min',mn,'name',name);
        end
    end;
end;
return;
%_______________________________________________________________________
%_______________________________________________________________________
function addimage(handle, fname)
global st
for i=valid_handles(handle),
    if isstruct(fname),
        vol = fname(1);
    else
        vol = spm_vol(fname);
    end;
    mat = vol.mat;
    st.vols{i}.blobs=cell(1,1);
    mx = max([eps maxval(vol)]);
    mn = min([0 minval(vol)]);
    st.vols{i}.blobs{1} = struct('vol',vol,'mat',mat,'max',mx,'min',mn);
end;
return;
%_______________________________________________________________________
%_______________________________________________________________________
function addcolouredblobs(handle, xyz, t, mat, colour, name)
if nargin < 6
    name = '';
end;
global st
for i=valid_handles(handle),
    if ~isempty(xyz),
        rcp      = round(xyz);
        dim      = max(rcp,[],2)';
        off      = rcp(1,:) + dim(1)*(rcp(2,:)-1 + dim(2)*(rcp(3,:)-1));
        vol      = zeros(dim)+NaN;
        vol(off) = t;
        vol      = reshape(vol,dim);
        if ~isfield(st.vols{i},'blobs'),
            st.vols{i}.blobs=cell(1,1);
            bset = 1;
        else
            bset = numel(st.vols{i}.blobs)+1;
        end;
        mx = max([eps maxval(vol)]);
        mn = min([0 minval(vol)]);
        st.vols{i}.blobs{bset} = struct('vol',vol, 'mat',mat, ...
            'max',mx, 'min',mn, ...
            'colour',colour, 'name',name);
    end;
end;
return;
%_______________________________________________________________________
%_______________________________________________________________________
function addcolouredimage(handle, fname,colour)
global st
for i=valid_handles(handle),
    if isstruct(fname),
        vol = fname(1);
    else
        vol = spm_vol(fname);
    end;
    mat = vol.mat;
    if ~isfield(st.vols{i},'blobs'),
        st.vols{i}.blobs=cell(1,1);
        bset = 1;
    else
        bset = numel(st.vols{i}.blobs)+1;
    end;
    mx = max([eps maxval(vol)]);
    mn = min([0 minval(vol)]);
    st.vols{i}.blobs{bset} = struct('vol',vol,'mat',mat,'max',mx,'min',mn,'colour',colour);
end;
return;
%_______________________________________________________________________
%_______________________________________________________________________
function addtruecolourimage(handle,fname,colourmap,prop,mx,mn)
% adds true colour image to current displayed image
global st
for i=valid_handles(handle),
    if isstruct(fname),
        vol = fname(1);
    else
        vol = spm_vol(fname);
    end;
    mat = vol.mat;
    if ~isfield(st.vols{i},'blobs'),
        st.vols{i}.blobs=cell(1,1);
        bset = 1;
    else
        bset = numel(st.vols{i}.blobs)+1;
    end;
    c = struct('cmap', colourmap,'prop',prop);
    st.vols{i}.blobs{bset} = struct('vol',vol,'mat',mat,'max',mx, ...
        'min',mn,'colour',c);
end;
return;
%_______________________________________________________________________
%_______________________________________________________________________
function rmblobs(handle)
global st
for i=valid_handles(handle),
    if isfield(st.vols{i},'blobs'),
        for j=1:numel(st.vols{i}.blobs),
            if isfield(st.vols{i}.blobs{j},'cbar') && ishandle(st.vols{i}.blobs{j}.cbar),
                delete(st.vols{i}.blobs{j}.cbar);
            end;
        end;
        st.vols{i} = rmfield(st.vols{i},'blobs');
    end;
end;
return;
%_______________________________________________________________________
%_______________________________________________________________________
function register(hreg)
global st
tmp = uicontrol('Position',[0 0 1 1],'Visible','off','Parent',st.fig);
h   = valid_handles(1:24);
if ~isempty(h),
    tmp = st.vols{h(1)}.ax{1}.ax;
    st.registry = struct('hReg',hreg,'hMe', tmp);
    spm_XYZreg('Add2Reg',st.registry.hReg,st.registry.hMe, 'se_orthviews');
else
    warning('Nothing to register with');
end;
st.centre = spm_XYZreg('GetCoords',st.registry.hReg);
st.centre = st.centre(:);
return;
%_______________________________________________________________________
%_______________________________________________________________________
function xhairs(arg1)
global st
st.xhairs = 0;
opt = 'on';
if ~strcmp(arg1,'on'),
    opt = 'off';
else
    st.xhairs = 1;
end;
for i=valid_handles(1:24),
    for j=1:3,
        set(st.vols{i}.ax{j}.lx,'Visible',opt);
        set(st.vols{i}.ax{j}.ly,'Visible',opt);
    end;
end;
return;
%_______________________________________________________________________
%_______________________________________________________________________
function H = pos(arg1)
global st
H = [];
for arg1=valid_handles(arg1),
    is = inv(st.vols{arg1}.premul*st.vols{arg1}.mat);
    H = is(1:3,1:3)*st.centre(:) + is(1:3,4);
end;
return;
%_______________________________________________________________________
%_______________________________________________________________________
function my_reset
global st

try
    keepMove = st.move;
end

if ~isempty(st) && isfield(st,'registry') && ishandle(st.registry.hMe),
    delete(st.registry.hMe); st = rmfield(st,'registry');
end;
my_delete(1:24);
reset_st;

try
    st.move = keepMove;
end
return;
%_______________________________________________________________________
%_______________________________________________________________________
function my_delete(arg1)
global st
for i=valid_handles(arg1),
    kids = get(st.fig,'Children');
    for j=1:3,
        if any(kids == st.vols{i}.ax{j}.ax),
            set(get(st.vols{i}.ax{j}.ax,'Children'),'DeleteFcn','');
            delete(st.vols{i}.ax{j}.ax);
        end;
    end;
    st.vols{i} = [];
end;
return;
%_______________________________________________________________________
%_______________________________________________________________________
function resolution(arg1)
global st
res      = arg1/mean(svd(st.Space(1:3,1:3)));
Mat      = diag([res res res 1]);
st.Space = st.Space*Mat;
st.bb    = st.bb/res;
return;
%_______________________________________________________________________
%_______________________________________________________________________
function move(handle,pos)
global st
for handle = valid_handles(handle),
    st.vols{handle}.area = pos;
end;
bbox;
% redraw(valid_handles(handle));
return;
%_______________________________________________________________________
%_______________________________________________________________________
function bb = maxbb
global st
mn = [Inf Inf Inf];
mx = -mn;
for i=valid_handles(1:24),
    bb = [[1 1 1];st.vols{i}.dim(1:3)];
    c = [   bb(1,1) bb(1,2) bb(1,3) 1
        bb(1,1) bb(1,2) bb(2,3) 1
        bb(1,1) bb(2,2) bb(1,3) 1
        bb(1,1) bb(2,2) bb(2,3) 1
        bb(2,1) bb(1,2) bb(1,3) 1
        bb(2,1) bb(1,2) bb(2,3) 1
        bb(2,1) bb(2,2) bb(1,3) 1
        bb(2,1) bb(2,2) bb(2,3) 1]';
    tc = st.Space\(st.vols{i}.premul*st.vols{i}.mat)*c;
    tc = tc(1:3,:)';
    mx = max([tc ; mx]);
    mn = min([tc ; mn]);
end;
bb = [mn ; mx];
return;
%_______________________________________________________________________
%_______________________________________________________________________
function space(arg1)
global st
if ~isempty(st.vols{arg1})
    num = arg1;
    Mat = st.vols{num}.premul(1:3,1:3)*st.vols{num}.mat(1:3,1:3);
    Mat = diag([sqrt(sum(Mat.^2)) 1]);
    Space = (st.vols{num}.mat)/Mat;
    bb = [1 1 1;st.vols{num}.dim(1:3)];
    bb = [bb [1;1]];
    bb=bb*Mat';
    bb=bb(:,1:3);
    st.Space  = Space;
    st.bb = bb;
end;
return;
%_______________________________________________________________________
%_______________________________________________________________________
function H = specify_image(arg1)
global st
H=[];
if isstruct(arg1),
    V = arg1(1);
else
    try
        V = spm_vol(arg1);
    catch
        fprintf('Can not use image "%s"\n', arg1);
        return;
    end;
end;
if st.move
    V.mat(2,4) = V.mat(2,4) - 4;
    V.mat(3,4) = V.mat(3,4) + 5;
    
    V.private.mat(2,4) = V.private.mat(2,4) - 4;
    V.private.mat(3,4) = V.private.mat(3,4) + 5;
    
end 
st.move = 0;

ii = 1;
while ~isempty(st.vols{ii}), ii = ii + 1; end;

DeleteFcn = ['se_orthviews(''Delete'',' num2str(ii) ');'];
V.ax = cell(3,1);
for i=1:3,
    ax = axes('Visible','off','DrawMode','fast','Parent',st.fig,'DeleteFcn',DeleteFcn,...
        'YDir','normal','ButtonDownFcn',@repos_start);
    d  = image(0,'Tag','Transverse','Parent',ax,...
        'DeleteFcn',DeleteFcn);
    set(ax,'Ydir','normal','ButtonDownFcn',@repos_start);
    
    lx = line(0,0,'Parent',ax,'DeleteFcn',DeleteFcn);
    ly = line(0,0,'Parent',ax,'DeleteFcn',DeleteFcn);
    if ~st.xhairs,
        set(lx,'Visible','off');
        set(ly,'Visible','off');
    end;
    V.ax{i} = struct('ax',ax,'d',d,'lx',lx,'ly',ly);
end;
V.premul    = eye(4);
V.window    = 'auto';
V.mapping   = 'linear';
st.vols{ii} = V;

H = ii;
return;
%_______________________________________________________________________
%_______________________________________________________________________
function repos_start(varargin)
% don't use right mouse button to start reposition
if ~strcmpi(get(gcbf,'SelectionType'),'alt')
    set(gcbf,'windowbuttonmotionfcn',@repos_move, 'windowbuttonupfcn',@repos_end);
    se_orthviews('reposition');
end
%_______________________________________________________________________
%_______________________________________________________________________
function repos_move(varargin)
se_orthviews('reposition');
%_______________________________________________________________________
%_______________________________________________________________________
function repos_end(varargin)
set(gcbf,'windowbuttonmotionfcn','', 'windowbuttonupfcn','');
%_______________________________________________________________________
%_______________________________________________________________________
function bbox
global st
Dims = diff(st.bb)'+1;

TD = Dims([1 2])';
CD = Dims([1 3])';
if st.mode == 0, SD = Dims([3 2])'; else SD = Dims([2 3])'; end;

un    = get(st.fig,'Units');set(st.fig,'Units','Pixels');
sz    = get(st.fig,'Position');set(st.fig,'Units',un);
sz    = sz(3:4);
sz(2) = sz(2)-40;

for i=valid_handles(1:24),
    area = st.vols{i}.area(:);
    area = [area(1)*sz(1) area(2)*sz(2) area(3)*sz(1) area(4)*sz(2)];
    if st.mode == 0,
        sx   = area(3)/(Dims(1)+Dims(3))/1.02;
    else
        sx   = area(3)/(Dims(1)+Dims(2))/1.02;
    end;
    sy   = area(4)/(Dims(2)+Dims(3))/1.02;
    s    = min([sx sy]);
    
    offy = (area(4)-(Dims(2)+Dims(3))*1.02*s)/2 + area(2);
    sky = s*(Dims(2)+Dims(3))*0.02;
    if st.mode == 0,
        offx = (area(3)-(Dims(1)+Dims(3))*1.02*s)/2 + area(1);
        skx = s*(Dims(1)+Dims(3))*0.02;
    else
        offx = (area(3)-(Dims(1)+Dims(2))*1.02*s)/2 + area(1);
        skx = s*(Dims(1)+Dims(2))*0.02;
    end;
    
    % Transverse
    set(st.vols{i}.ax{1}.ax,'Units','pixels', ...
        'Position',[offx offy s*Dims(1) s*Dims(2)],...
        'Units','normalized','Xlim',[0 TD(1)]+0.5,'Ylim',[0 TD(2)]+0.5,...
        'Visible','on','XTick',[],'YTick',[]);
    
    % Coronal
    set(st.vols{i}.ax{2}.ax,'Units','Pixels',...
        'Position',[offx offy+s*Dims(2)+sky s*Dims(1) s*Dims(3)],...
        'Units','normalized','Xlim',[0 CD(1)]+0.5,'Ylim',[0 CD(2)]+0.5,...
        'Visible','on','XTick',[],'YTick',[]);
    
    % Sagittal
    if st.mode == 0,
        set(st.vols{i}.ax{3}.ax,'Units','Pixels', 'Box','on',...
            'Position',[offx+s*Dims(1)+skx offy s*Dims(3) s*Dims(2)],...
            'Units','normalized','Xlim',[0 SD(1)]+0.5,'Ylim',[0 SD(2)]+0.5,...
            'Visible','on','XTick',[],'YTick',[]);
    else
        set(st.vols{i}.ax{3}.ax,'Units','Pixels', 'Box','on',...
            'Position',[offx+s*Dims(1)+skx offy+s*Dims(2)+sky s*Dims(2) s*Dims(3)],...
            'Units','normalized','Xlim',[0 SD(1)]+0.5,'Ylim',[0 SD(2)]+0.5,...
            'Visible','on','XTick',[],'YTick',[]);
    end;
end;
return;
%_______________________________________________________________________
%_______________________________________________________________________
function redraw_all
redraw(1:24);
return;
%_______________________________________________________________________
function mx = maxval(vol)
if isstruct(vol),
    mx = -Inf;
    for i=1:vol.dim(3),
        tmp = spm_slice_vol(vol,spm_matrix([0 0 i]),vol.dim(1:2),0);
        imx = max(tmp(isfinite(tmp)));
        if ~isempty(imx),mx = max(mx,imx);end
    end;
else
    mx = max(vol(isfinite(vol)));
end;
%_______________________________________________________________________
function mn = minval(vol)
if isstruct(vol),
    mn = Inf;
    for i=1:vol.dim(3),
        tmp = spm_slice_vol(vol,spm_matrix([0 0 i]),vol.dim(1:2),0);
        imn = min(tmp(isfinite(tmp)));
        if ~isempty(imn),mn = min(mn,imn);end
    end;
else
    mn = min(vol(isfinite(vol)));
end;

%_______________________________________________________________________
%_______________________________________________________________________
function redraw(arg1)
global st
global displayType;
bb   = st.bb;
Dims = round(diff(bb)'+1);
is   = inv(st.Space);
cent = is(1:3,1:3)*st.centre(:) + is(1:3,4);

for i = valid_handles(arg1),
    M = st.vols{i}.premul*st.vols{i}.mat;
    TM0 = [ 1 0 0 -bb(1,1)+1
        0 1 0 -bb(1,2)+1
        0 0 1 -cent(3)
        0 0 0 1];
    TM = inv(TM0*(st.Space\M));
    TD = Dims([1 2]);
    
    CM0 = [ 1 0 0 -bb(1,1)+1
        0 0 1 -bb(1,3)+1
        0 1 0 -cent(2)
        0 0 0 1];
    CM = inv(CM0*(st.Space\M));
    CD = Dims([1 3]);
    
    if st.mode ==0,
        SM0 = [ 0 0 1 -bb(1,3)+1
            0 1 0 -bb(1,2)+1
            1 0 0 -cent(1)
            0 0 0 1];
        SM = inv(SM0*(st.Space\M)); SD = Dims([3 2]);
    else
        SM0 = [ 0 -1 0 +bb(2,2)+1
            0  0 1 -bb(1,3)+1
            1  0 0 -cent(1)
            0  0 0 1];
        SM = inv(SM0*(st.Space\M));
        SD = Dims([2 3]);
    end;
    
    try
        imgt  = spm_slice_vol(st.vols{i},TM,TD,st.hld)';
        imgc  = spm_slice_vol(st.vols{i},CM,CD,st.hld)';
        imgs  = spm_slice_vol(st.vols{i},SM,SD,st.hld)';
        ok    = true;
    catch
        fprintf('Image "%s" can not be resampled\n', st.vols{i}.fname);
        ok     = false;
    end
    if ok,
        % get min/max threshold
        if strcmp(st.vols{i}.window,'auto')
            mn = -Inf;
            mx = Inf;
        else
            mn = min(st.vols{i}.window);
            mx = max(st.vols{i}.window);
        end;
        % threshold images
        imgt = max(imgt,mn); imgt = min(imgt,mx);
        imgc = max(imgc,mn); imgc = min(imgc,mx);
        imgs = max(imgs,mn); imgs = min(imgs,mx);
        % compute intensity mapping, if histeq is available
        if license('test','image_toolbox') == 0
            st.vols{i}.mapping = 'linear';
        end;
        switch st.vols{i}.mapping,
            case 'linear',
            case 'histeq',
                % scale images to a range between 0 and 1
                imgt1=(imgt-min(imgt(:)))/(max(imgt(:)-min(imgt(:)))+eps);
                imgc1=(imgc-min(imgc(:)))/(max(imgc(:)-min(imgc(:)))+eps);
                imgs1=(imgs-min(imgs(:)))/(max(imgs(:)-min(imgs(:)))+eps);
                img  = histeq([imgt1(:); imgc1(:); imgs1(:)],1024);
                imgt = reshape(img(1:numel(imgt1)),size(imgt1));
                imgc = reshape(img(numel(imgt1)+(1:numel(imgc1))),size(imgc1));
                imgs = reshape(img(numel(imgt1)+numel(imgc1)+(1:numel(imgs1))),size(imgs1));
                mn = 0;
                mx = 1;
            case 'quadhisteq',
                % scale images to a range between 0 and 1
                imgt1=(imgt-min(imgt(:)))/(max(imgt(:)-min(imgt(:)))+eps);
                imgc1=(imgc-min(imgc(:)))/(max(imgc(:)-min(imgc(:)))+eps);
                imgs1=(imgs-min(imgs(:)))/(max(imgs(:)-min(imgs(:)))+eps);
                img  = histeq([imgt1(:).^2; imgc1(:).^2; imgs1(:).^2],1024);
                imgt = reshape(img(1:numel(imgt1)),size(imgt1));
                imgc = reshape(img(numel(imgt1)+(1:numel(imgc1))),size(imgc1));
                imgs = reshape(img(numel(imgt1)+numel(imgc1)+(1:numel(imgs1))),size(imgs1));
                mn = 0;
                mx = 1;
            case 'loghisteq',
                sw = warning('off','MATLAB:log:logOfZero');
                imgt = log(imgt-min(imgt(:)));
                imgc = log(imgc-min(imgc(:)));
                imgs = log(imgs-min(imgs(:)));
                warning(sw);
                imgt(~isfinite(imgt)) = 0;
                imgc(~isfinite(imgc)) = 0;
                imgs(~isfinite(imgs)) = 0;
                % scale log images to a range between 0 and 1
                imgt1=(imgt-min(imgt(:)))/(max(imgt(:)-min(imgt(:)))+eps);
                imgc1=(imgc-min(imgc(:)))/(max(imgc(:)-min(imgc(:)))+eps);
                imgs1=(imgs-min(imgs(:)))/(max(imgs(:)-min(imgs(:)))+eps);
                img  = histeq([imgt1(:); imgc1(:); imgs1(:)],1024);
                imgt = reshape(img(1:numel(imgt1)),size(imgt1));
                imgc = reshape(img(numel(imgt1)+(1:numel(imgc1))),size(imgc1));
                imgs = reshape(img(numel(imgt1)+numel(imgc1)+(1:numel(imgs1))),size(imgs1));
                mn = 0;
                mx = 1;
        end;
        % recompute min/max for display
        if strcmp(st.vols{i}.window,'auto')
            mx = -inf; mn = inf;
        end;
        if ~isempty(imgt),
            tmp = imgt(isfinite(imgt));
            mx = max([mx max(max(tmp))]);
            mn = min([mn min(min(tmp))]);
        end;
        if ~isempty(imgc),
            tmp = imgc(isfinite(imgc));
            mx = max([mx max(max(tmp))]);
            mn = min([mn min(min(tmp))]);
        end;
        if ~isempty(imgs),
            tmp = imgs(isfinite(imgs));
            mx = max([mx max(max(tmp))]);
            mn = min([mn min(min(tmp))]);
        end;
        if mx==mn, mx=mn+eps; end;
        
        if isfield(st.vols{i},'blobs'),
            if ~isfield(st.vols{i}.blobs{1},'colour'),
                % Add blobs for display using the split colourmap
                scal = 64/(mx-mn);
                dcoff = -mn*scal;
                imgt = imgt*scal+dcoff;
                imgc = imgc*scal+dcoff;
                imgs = imgs*scal+dcoff;
                
                if isfield(st.vols{i}.blobs{1},'max'),
                    mx = st.vols{i}.blobs{1}.max;
                else
                    mx = max([eps maxval(st.vols{i}.blobs{1}.vol)]);
                    st.vols{i}.blobs{1}.max = mx;
                end;
                if isfield(st.vols{i}.blobs{1},'min'),
                    mn = st.vols{i}.blobs{1}.min;
                else
                    mn = min([0 minval(st.vols{i}.blobs{1}.vol)]);
                    st.vols{i}.blobs{1}.min = mn;
                end;
                
                vol  = st.vols{i}.blobs{1}.vol;
                M    = st.vols{i}.premul*st.vols{i}.blobs{1}.mat;
                tmpt = spm_slice_vol(vol,inv(TM0*(st.Space\M)),TD,[0 NaN])';
                tmpc = spm_slice_vol(vol,inv(CM0*(st.Space\M)),CD,[0 NaN])';
                tmps = spm_slice_vol(vol,inv(SM0*(st.Space\M)),SD,[0 NaN])';
                
                if strcmp(displayType,'PM')
                    sc   = 100/(mx-mn);
                else
                    sc   = 64/(mx-mn);
                end
                off  = 65.51-mn*sc;
                msk  = find(isfinite(tmpt)); imgt(msk) = off+tmpt(msk)*sc;
                msk  = find(isfinite(tmpc)); imgc(msk) = off+tmpc(msk)*sc;
                msk  = find(isfinite(tmps)); imgs(msk) = off+tmps(msk)*sc;
                
                if strcmp(displayType,'PM')
                    figure(st.fig)
                    se_figure('Colormap','gray-jet')
                else
                    cmap = get(st.fig,'Colormap');
                    if size(cmap,1)~=128
                    	figure(st.fig)
                        se_figure('Colormap','gray-hot')
                    end;
                end
            elseif isstruct(st.vols{i}.blobs{1}.colour),
                % Add blobs for display using a defined
                % colourmap
                
                % colourmaps
                gryc = (0:63)'*ones(1,3)/63;
                actc = ...
                    st.vols{1}.blobs{1}.colour.cmap;
                actp = ...
                    st.vols{1}.blobs{1}.colour.prop;
                
                % scale grayscale image, not isfinite -> black
                imgt = scaletocmap(imgt,mn,mx,gryc,65);
                imgc = scaletocmap(imgc,mn,mx,gryc,65);
                imgs = scaletocmap(imgs,mn,mx,gryc,65);
                gryc = [gryc; 0 0 0];
                
                % get max for blob image
                if isfield(st.vols{i}.blobs{1},'max'),
                    cmx = st.vols{i}.blobs{1}.max;
                else
                    cmx = max([eps maxval(st.vols{i}.blobs{1}.vol)]);
                end;
                if isfield(st.vols{i}.blobs{1},'min'),
                    cmn = st.vols{i}.blobs{1}.min;
                else
                    cmn = -cmx;
                end;
                
                % get blob data
                vol  = st.vols{i}.blobs{1}.vol;
                M    = st.vols{i}.premul*st.vols{i}.blobs{1}.mat;
                tmpt = spm_slice_vol(vol,inv(TM0*(st.Space\M)),TD,[0 NaN])';
                tmpc = spm_slice_vol(vol,inv(CM0*(st.Space\M)),CD,[0 NaN])';
                tmps = spm_slice_vol(vol,inv(SM0*(st.Space\M)),SD,[0 NaN])';
                
                % actimg scaled round 0, black NaNs
                topc = size(actc,1)+1;
                tmpt = scaletocmap(tmpt,cmn,cmx,actc,topc);
                tmpc = scaletocmap(tmpc,cmn,cmx,actc,topc);
                tmps = scaletocmap(tmps,cmn,cmx,actc,topc);
                actc = [actc; 0 0 0];
                
                % combine gray and blob data to
                % truecolour
                imgt = reshape(actc(tmpt(:),:)*actp+ ...
                    gryc(imgt(:),:)*(1-actp), ...
                    [size(imgt) 3]);
                imgc = reshape(actc(tmpc(:),:)*actp+ ...
                    gryc(imgc(:),:)*(1-actp), ...
                    [size(imgc) 3]);
                imgs = reshape(actc(tmps(:),:)*actp+ ...
                    gryc(imgs(:),:)*(1-actp), ...
                    [size(imgs) 3]);
                
            else
                % Add full colour blobs - several sets at once
                scal  = 1/(mx-mn);
                dcoff = -mn*scal;
                
                wt = zeros(size(imgt));
                wc = zeros(size(imgc));
                ws = zeros(size(imgs));
                
                imgt  = repmat(imgt*scal+dcoff,[1,1,3]);
                imgc  = repmat(imgc*scal+dcoff,[1,1,3]);
                imgs  = repmat(imgs*scal+dcoff,[1,1,3]);
                
                cimgt = zeros(size(imgt));
                cimgc = zeros(size(imgc));
                cimgs = zeros(size(imgs));
                
                colour = zeros(numel(st.vols{i}.blobs),3);
                for j=1:numel(st.vols{i}.blobs) % get colours of all images first
                    if isfield(st.vols{i}.blobs{j},'colour'),
                        colour(j,:) = reshape(st.vols{i}.blobs{j}.colour, [1 3]);
                    else
                        colour(j,:) = [1 0 0];
                    end;
                end;
                %colour = colour/max(sum(colour));
                
                for j=1:numel(st.vols{i}.blobs),
                    if isfield(st.vols{i}.blobs{j},'max'),
                        mx = st.vols{i}.blobs{j}.max;
                    else
                        mx = max([eps max(st.vols{i}.blobs{j}.vol(:))]);
                        st.vols{i}.blobs{j}.max = mx;
                    end;
                    if isfield(st.vols{i}.blobs{j},'min'),
                        mn = st.vols{i}.blobs{j}.min;
                    else
                        mn = min([0 min(st.vols{i}.blobs{j}.vol(:))]);
                        st.vols{i}.blobs{j}.min = mn;
                    end;
                    
                    vol  = st.vols{i}.blobs{j}.vol;
                    M    = st.Space\st.vols{i}.premul*st.vols{i}.blobs{j}.mat;
                    tmpt = spm_slice_vol(vol,inv(TM0*M),TD,[0 NaN])';
                    tmpc = spm_slice_vol(vol,inv(CM0*M),CD,[0 NaN])';
                    tmps = spm_slice_vol(vol,inv(SM0*M),SD,[0 NaN])';
                    % check min/max of sampled image
                    % against mn/mx as given in st
                    tmpt(tmpt(:)<mn) = mn;
                    tmpc(tmpc(:)<mn) = mn;
                    tmps(tmps(:)<mn) = mn;
                    tmpt(tmpt(:)>mx) = mx;
                    tmpc(tmpc(:)>mx) = mx;
                    tmps(tmps(:)>mx) = mx;
                    tmpt = (tmpt-mn)/(mx-mn);
                    tmpc = (tmpc-mn)/(mx-mn);
                    tmps = (tmps-mn)/(mx-mn);
                    tmpt(~isfinite(tmpt)) = 0;
                    tmpc(~isfinite(tmpc)) = 0;
                    tmps(~isfinite(tmps)) = 0;
                    
                    cimgt = cimgt + cat(3,tmpt*colour(j,1),tmpt*colour(j,2),tmpt*colour(j,3));
                    cimgc = cimgc + cat(3,tmpc*colour(j,1),tmpc*colour(j,2),tmpc*colour(j,3));
                    cimgs = cimgs + cat(3,tmps*colour(j,1),tmps*colour(j,2),tmps*colour(j,3));
                    
                    wt = wt + tmpt;
                    wc = wc + tmpc;
                    ws = ws + tmps;
                end;
                
                imgt = repmat(1-wt,[1 1 3]).*imgt+cimgt;
                imgc = repmat(1-wc,[1 1 3]).*imgc+cimgc;
                imgs = repmat(1-ws,[1 1 3]).*imgs+cimgs;
                
                imgt(imgt<0)=0; imgt(imgt>1)=1;
                imgc(imgc<0)=0; imgc(imgc>1)=1;
                imgs(imgs<0)=0; imgs(imgs>1)=1;
            end;
        else
            scal = 64/(mx-mn);
            dcoff = -mn*scal;
            imgt = imgt*scal+dcoff;
            imgc = imgc*scal+dcoff;
            imgs = imgs*scal+dcoff;
        end;
        
        set(st.vols{i}.ax{1}.d,'HitTest','off', 'Cdata',imgt);
        set(st.vols{i}.ax{1}.lx,'HitTest','off',...
            'Xdata',[0 TD(1)]+0.5,'Ydata',[1 1]*(cent(2)-bb(1,2)+1));
        set(st.vols{i}.ax{1}.ly,'HitTest','off',...
            'Ydata',[0 TD(2)]+0.5,'Xdata',[1 1]*(cent(1)-bb(1,1)+1));
        
        set(st.vols{i}.ax{2}.d,'HitTest','off', 'Cdata',imgc);
        set(st.vols{i}.ax{2}.lx,'HitTest','off',...
            'Xdata',[0 CD(1)]+0.5,'Ydata',[1 1]*(cent(3)-bb(1,3)+1));
        set(st.vols{i}.ax{2}.ly,'HitTest','off',...
            'Ydata',[0 CD(2)]+0.5,'Xdata',[1 1]*(cent(1)-bb(1,1)+1));
        
        set(st.vols{i}.ax{3}.d,'HitTest','off','Cdata',imgs);
        if st.mode ==0,
            set(st.vols{i}.ax{3}.lx,'HitTest','off',...
                'Xdata',[0 SD(1)]+0.5,'Ydata',[1 1]*(cent(2)-bb(1,2)+1));
            set(st.vols{i}.ax{3}.ly,'HitTest','off',...
                'Ydata',[0 SD(2)]+0.5,'Xdata',[1 1]*(cent(3)-bb(1,3)+1));
        else
            set(st.vols{i}.ax{3}.lx,'HitTest','off',...
                'Xdata',[0 SD(1)]+0.5,'Ydata',[1 1]*(cent(3)-bb(1,3)+1));
            set(st.vols{i}.ax{3}.ly,'HitTest','off',...
                'Ydata',[0 SD(2)]+0.5,'Xdata',[1 1]*(bb(2,2)+1-cent(2)));
        end;
    end;
end;
drawnow;
return;
%_______________________________________________________________________
%_______________________________________________________________________
function centre = findcent
global st
obj    = get(st.fig,'CurrentObject');
centre = [];
cent   = [];
cp     = [];
for i=valid_handles(1:24),
    for j=1:3,
        if ~isempty(obj),
            if (st.vols{i}.ax{j}.ax == obj),
                cp = get(obj,'CurrentPoint');
            end;
        end;
        if ~isempty(cp),
            cp   = cp(1,1:2);
            is   = inv(st.Space);
            cent = is(1:3,1:3)*st.centre(:) + is(1:3,4);
            switch j,
                case 1,
                    cent([1 2])=[cp(1)+st.bb(1,1)-1 cp(2)+st.bb(1,2)-1];
                case 2,
                    cent([1 3])=[cp(1)+st.bb(1,1)-1 cp(2)+st.bb(1,3)-1];
                case 3,
                    if st.mode ==0,
                        cent([3 2])=[cp(1)+st.bb(1,3)-1 cp(2)+st.bb(1,2)-1];
                    else
                        cent([2 3])=[st.bb(2,2)+1-cp(1) cp(2)+st.bb(1,3)-1];
                    end;
            end;
            break;
        end;
    end;
    if ~isempty(cent), break; end;
end;
if ~isempty(cent), centre = st.Space(1:3,1:3)*cent(:) + st.Space(1:3,4); end;
return;
%_______________________________________________________________________
%_______________________________________________________________________
function handles = valid_handles(handles)
global st;
if isempty(st) || ~isfield(st,'vols')
    handles = [];
else
    handles = handles(:)';
    handles = handles(handles<=24 & handles>=1 & ~rem(handles,1));
    for h=handles,
        if isempty(st.vols{h}), handles(handles==h)=[]; end;
    end;
end;
return;
%_______________________________________________________________________
%_______________________________________________________________________
function reset_st
global st
fig     = se_figure('FindWin','Graphics');
bb      = [ [-78 78]' [-112 76]' [-50 85]' ];
st      = struct('n', 0, 'vols',[], 'bb',bb,'Space',eye(4),'centre',[0 0 0],'callback',';','xhairs',1,'hld',1,'fig',fig,'mode',1,'move',0);
st.vols = cell(24,1);
return;
%_______________________________________________________________________
%_______________________________________________________________________
function img = scaletocmap(inpimg,mn,mx,cmap,miscol)
if nargin < 5, miscol=1;end
cml = size(cmap,1);
scf = (cml-1)/(mx-mn);
img = round((inpimg-mn)*scf)+1;
img(img<1)   = 1;
img(img>cml) = cml;
img(~isfinite(img))  = miscol;
return;
%_______________________________________________________________________
%_______________________________________________________________________
function cmap = getcmap(acmapname)
% get colormap of name acmapname
if ~isempty(acmapname),
    cmap = evalin('base',acmapname,'[]');
    if isempty(cmap), % not a matrix, is .mat file?
        [p, f, e] = fileparts(acmapname);
        acmat     = fullfile(p, [f '.mat']);
        if exist(acmat, 'file'),
            s    = struct2cell(load(acmat));
            cmap = s{1};
        end;
    end;
end;
if size(cmap, 2)~=3,
    warning('Colormap was not an N by 3 matrix')
    cmap = [];
end;
return;
