function se_preComp

global st;
global MAP;
global SPM;
global xSPM;
global CLUSTER;


for cl=1:numel(CLUSTER)
    if ~isfield(CLUSTER,'gtmp') || prod(size(CLUSTER(cl).gtmp)) < 1
        tmp   = CLUSTER(cl).XYZp;
        tmpZ  = CLUSTER(cl).Z;
        tms = sign(CLUSTER(cl).XYZmm); 
        clear gtmp; gZ = []; mult = 1;
        for xbx = 0:.5:xSPM.xVOL.VOX(1)-1
            for ybx = 0:.5:xSPM.xVOL.VOX(2)-1
                for zbx = 0:.5:xSPM.xVOL.VOX(3)-1
                    if exist('gtmp')
                        gtmp = [gtmp(1,:) tmp(1,:)-(tms(1,:)*xbx);...
                                gtmp(2,:) tmp(2,:)-(tms(2,:)*ybx);...
                                gtmp(3,:) tmp(3,:)-(tms(3,:)*zbx)];
                        gZ   = [gZ tmpZ];
                    else
                        gtmp = [tmp(1,:)-(tms(1,:)*xbx);...
                                tmp(2,:)-(tms(2,:)*ybx);...
                                tmp(3,:)-(tms(3,:)*zbx)];
                            
                        gZ   = [tmpZ];
                    end
                end
            end
        end
        gtmp      = round(gtmp);
        [b, m, n] = unique(gtmp','rows');
        
        gtmp = gtmp(:,m);
        gZ   = gZ(:,m);
        
        CLUSTER(cl).gtmp = gtmp;
        CLUSTER(cl).gZ   = gZ;
        CLUSTER(cl).mult = length(gZ)/length(tmpZ);
    end
    try
        [PQ, Z, nP, index] = se_plotCorrespond2(CLUSTER(cl));
        CLUSTER(cl).PQ = PQ;
        CLUSTER(cl).xZ = Z;
        CLUSTER(cl).nP = nP;
        CLUSTER(cl).index = index;
        try; delete(st.FX); end
    end
    
        se_render(cl)
        fprintf(1,'%s\n',['Cluster ' int2str(cl) '/' int2str(numel(CLUSTER)) ' completed'])
end
spm('alert','All cluster-stats precomputed');
