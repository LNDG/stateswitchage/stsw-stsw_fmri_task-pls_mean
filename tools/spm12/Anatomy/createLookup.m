clear, clc

load AllAreasIntern_v15b_MPM.mat

fid = fopen('AllAreasIntern_v15b_MPM.txt','wt');

for i=1:size(MAP,2)
    name{i} = MAP(i).name;
    GV(i)   = MAP(i).GV;
end

[names idx] = sort(name);
GVs         = GV(idx);

for i=1:size(MAP,2)
     fprintf(fid,'%s\t%6.0f\t%6.0f\n',names{i},GVs(i),idx(i));
end
status = fclose(fid);